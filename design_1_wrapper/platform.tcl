# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct C:\Cristina\Year_4\licenta\sd_hdmi_system_10\soft\design_1_wrapper\platform.tcl
# 
# OR launch xsct and run below command.
# source C:\Cristina\Year_4\licenta\sd_hdmi_system_10\soft\design_1_wrapper\platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {design_1_wrapper}\
-hw {C:\Cristina\Year_4\licenta\sd_hdmi_system_10\design_1_wrapper.xsa}\
-out {C:/Cristina/Year_4/licenta/sd_hdmi_system_10/soft}

platform write
domain create -name {standalone_ps7_cortexa9_0} -display-name {standalone_ps7_cortexa9_0} -os {standalone} -proc {ps7_cortexa9_0} -runtime {cpp} -arch {32-bit} -support-app {hello_world}
platform generate -domains 
platform active {design_1_wrapper}
domain active {zynq_fsbl}
domain active {standalone_ps7_cortexa9_0}
platform generate -quick
domain active {zynq_fsbl}
bsp reload
bsp reload
bsp reload
domain active {standalone_ps7_cortexa9_0}
bsp setlib -name xilffs -ver 5.1
bsp setlib -name xilrsa -ver 1.7
bsp write
bsp reload
catch {bsp regenerate}
platform generate
platform generate -domains 
platform generate -domains zynq_fsbl 
platform generate -domains standalone_ps7_cortexa9_0,zynq_fsbl 
platform generate -domains standalone_ps7_cortexa9_0 
platform generate -domains standalone_ps7_cortexa9_0 
platform generate -domains standalone_ps7_cortexa9_0 
platform generate -domains standalone_ps7_cortexa9_0 
platform active {design_1_wrapper}
platform active {design_1_wrapper}
platform generate
platform generate -domains standalone_ps7_cortexa9_0,zynq_fsbl 
platform active {design_1_wrapper}
platform generate
platform config -updatehw {C:/Cristina/Year_4/licenta/sd_hdmi_system_10/design_1_wrapper.xsa}
platform active {design_1_wrapper}
platform active {design_1_wrapper}
platform active {design_1_wrapper}
platform generate
platform active {design_1_wrapper}
platform generate -domains standalone_ps7_cortexa9_0,zynq_fsbl 
platform generate -domains 
platform generate -domains 
platform generate -domains 
platform generate -domains 
platform active {design_1_wrapper}
bsp reload
domain active {zynq_fsbl}
bsp reload
platform active {design_1_wrapper}
domain active {zynq_fsbl}
bsp reload
platform active {design_1_wrapper}
platform active {design_1_wrapper}
platform active {design_1_wrapper}
bsp reload
domain active {zynq_fsbl}
bsp reload
bsp reload
domain active {standalone_ps7_cortexa9_0}
bsp reload
platform generate
platform generate
platform generate -domains 
platform generate
platform generate -domains 
platform generate -domains 
platform generate -domains 
platform generate -domains 
platform generate -domains 
platform generate -domains 
