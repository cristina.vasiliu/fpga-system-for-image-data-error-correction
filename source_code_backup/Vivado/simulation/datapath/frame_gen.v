`timescale 1ns / 1ps

module frame_gen #(
  parameter  DW = 32,
  parameter  X  = 32, // frame width
  parameter  Y  = 20  // frame height

 ) (
  input             clk  ,
  input             rstn ,
  input             ready,
  output [DW-1  :0] dat  ,
  output            sof  ,
  output            eol  ,
  output            val
);

 localparam XW = $clog2(X);
 localparam YW = $clog2(Y);


 reg [1:0]    st_nxt,   st_ff;
 reg [XW-1:0] xcnt_nxt, xcnt_ff;
 reg [YW-1:0] ycnt_nxt, ycnt_ff;
 reg [DW-1:0] dat_nxt,  dat_ff;
 reg          sof_nxt,  sof_ff;
 reg          eol_nxt,  eol_ff;
 reg          val_nxt,  val_ff;

 assign dat = dat_ff;
 assign sof = sof_ff;
 assign eol = eol_ff;
 assign val = val_ff;



 always @(*) begin
   st_nxt   = st_ff;
   xcnt_nxt = xcnt_ff;
   ycnt_nxt = ycnt_ff;
   dat_nxt  = dat_ff;
   sof_nxt  = sof_ff;
   eol_nxt  = eol_ff;
   val_nxt  = val_ff;


   if (ready) begin
     case (st_ff)
     0 : begin
       if (ycnt_ff==0) sof_nxt = 1;
       val_nxt = 1;
       dat_nxt = dat_ff+1;
       xcnt_nxt = 0;

       eol_nxt = 0;
       st_nxt = 1;
     end
     1 : begin
       sof_nxt = 0;
       val_nxt = 1;
       eol_nxt = 0;
       dat_nxt = dat_ff+1;
       xcnt_nxt = xcnt_ff + 1;
       if (xcnt_ff == X-2) begin
         eol_nxt = 1;
       end
       if (xcnt_ff == X-1) begin
         eol_nxt = 0;
         xcnt_nxt = 0;
         val_nxt = 0;
         ycnt_nxt = (ycnt_ff<Y)? ycnt_ff + 1 : 0;
         st_nxt = 0;
       end
     end


     endcase
   end
 end



  always @(posedge clk or negedge rstn) begin
    if (!rstn) begin
      st_ff   <= 0;
      xcnt_ff <= 0;
      ycnt_ff <= 0;
      dat_ff  <= 0;
      sof_ff  <= 0;
      eol_ff  <= 0;
      val_ff  <= 0;

    end else begin
      st_ff   <= st_nxt;
      xcnt_ff <= xcnt_nxt;
      ycnt_ff <= ycnt_nxt;
      dat_ff  <= dat_nxt;
      sof_ff  <= sof_nxt;
      eol_ff  <= eol_nxt;
      val_ff  <= val_nxt;
    end
  end


endmodule
