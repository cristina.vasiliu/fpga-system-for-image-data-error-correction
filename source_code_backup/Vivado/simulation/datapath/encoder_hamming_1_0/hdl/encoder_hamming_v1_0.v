
`timescale 1 ns / 1 ps

	module encoder_hamming_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface ctrl_s
		parameter integer C_ctrl_s_DATA_WIDTH	= 32,
		parameter integer C_ctrl_s_ADDR_WIDTH	= 4,

		// Parameters of Axi Slave Bus Interface in_s
		parameter integer C_in_s_TDATA_WIDTH	= 32,

		// Parameters of Axi Master Bus Interface out_m
		parameter integer C_out_m_TDATA_WIDTH	= 32,
		parameter integer C_out_m_START_COUNT	= 32
	)
	(
		// Users to add ports here
		output [C_ctrl_s_DATA_WIDTH-1:0]	r0,
		output [C_ctrl_s_DATA_WIDTH-1:0]	r1,
		output [C_ctrl_s_DATA_WIDTH-1:0]	r2,
		output [C_ctrl_s_DATA_WIDTH-1:0]	r3,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface ctrl_s
		input wire  ctrl_s_aclk,
		input wire  ctrl_s_aresetn,
		input wire [C_ctrl_s_ADDR_WIDTH-1 : 0] ctrl_s_awaddr,
		input wire [2 : 0] ctrl_s_awprot,
		input wire  ctrl_s_awvalid,
		output wire  ctrl_s_awready,
		input wire [C_ctrl_s_DATA_WIDTH-1 : 0] ctrl_s_wdata,
		input wire [(C_ctrl_s_DATA_WIDTH/8)-1 : 0] ctrl_s_wstrb,
		input wire  ctrl_s_wvalid,
		output wire  ctrl_s_wready,
		output wire [1 : 0] ctrl_s_bresp,
		output wire  ctrl_s_bvalid,
		input wire  ctrl_s_bready,
		input wire [C_ctrl_s_ADDR_WIDTH-1 : 0] ctrl_s_araddr,
		input wire [2 : 0] ctrl_s_arprot,
		input wire  ctrl_s_arvalid,
		output wire  ctrl_s_arready,
		output wire [C_ctrl_s_DATA_WIDTH-1 : 0] ctrl_s_rdata,
		output wire [1 : 0] ctrl_s_rresp,
		output wire  ctrl_s_rvalid,
		input wire  ctrl_s_rready,

		// Ports of Axi Slave Bus Interface in_s
		input wire  in_s_aclk,
		input wire  in_s_aresetn,
		output wire  in_s_tready,
		input wire [C_in_s_TDATA_WIDTH-1 : 0] in_s_tdata,
		input wire [(C_in_s_TDATA_WIDTH/8)-1 : 0] in_s_tstrb,
		input wire  in_s_tlast,
		input wire  in_s_tvalid,

		// Ports of Axi Master Bus Interface out_m
		input wire  out_m_aclk,
		input wire  out_m_aresetn,
		output wire  out_m_tvalid,
		output wire [C_out_m_TDATA_WIDTH-1 : 0] out_m_tdata,
		output wire [(C_out_m_TDATA_WIDTH/8)-1 : 0] out_m_tstrb,
		output wire  out_m_tlast,
		input wire  out_m_tready
	);
// Instantiation of Axi Bus Interface ctrl_s
	encoder_hamming_v1_0_ctrl_s # ( 
		.C_S_AXI_DATA_WIDTH(C_ctrl_s_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_ctrl_s_ADDR_WIDTH)
	) encoder_hamming_v1_0_ctrl_s_inst (
	    .r0(r0),
		.r1(r1),
		.r2(r2),
		.r3(r3),
		.S_AXI_ACLK(ctrl_s_aclk),
		.S_AXI_ARESETN(ctrl_s_aresetn),
		.S_AXI_AWADDR(ctrl_s_awaddr),
		.S_AXI_AWPROT(ctrl_s_awprot),
		.S_AXI_AWVALID(ctrl_s_awvalid),
		.S_AXI_AWREADY(ctrl_s_awready),
		.S_AXI_WDATA(ctrl_s_wdata),
		.S_AXI_WSTRB(ctrl_s_wstrb),
		.S_AXI_WVALID(ctrl_s_wvalid),
		.S_AXI_WREADY(ctrl_s_wready),
		.S_AXI_BRESP(ctrl_s_bresp),
		.S_AXI_BVALID(ctrl_s_bvalid),
		.S_AXI_BREADY(ctrl_s_bready),
		.S_AXI_ARADDR(ctrl_s_araddr),
		.S_AXI_ARPROT(ctrl_s_arprot),
		.S_AXI_ARVALID(ctrl_s_arvalid),
		.S_AXI_ARREADY(ctrl_s_arready),
		.S_AXI_RDATA(ctrl_s_rdata),
		.S_AXI_RRESP(ctrl_s_rresp),
		.S_AXI_RVALID(ctrl_s_rvalid),
		.S_AXI_RREADY(ctrl_s_rready)
	);

// Instantiation of Axi Bus Interface in_s
/*
	encoder_hamming_v1_0_in_s # ( 
		.C_S_AXIS_TDATA_WIDTH(C_in_s_TDATA_WIDTH)
	) encoder_hamming_v1_0_in_s_inst (
		.S_AXIS_ACLK(in_s_aclk),
		.S_AXIS_ARESETN(in_s_aresetn),
		.S_AXIS_TREADY(in_s_tready),
		.S_AXIS_TDATA(in_s_tdata),
		.S_AXIS_TSTRB(in_s_tstrb),
		.S_AXIS_TLAST(in_s_tlast),
		.S_AXIS_TVALID(in_s_tvalid)
	);
*/
// Instantiation of Axi Bus Interface out_m
/*
	encoder_hamming_v1_0_out_m # ( 
		.C_M_AXIS_TDATA_WIDTH(C_out_m_TDATA_WIDTH),
		.C_M_START_COUNT(C_out_m_START_COUNT)
	) encoder_hamming_v1_0_out_m_inst (
		.M_AXIS_ACLK(out_m_aclk),
		.M_AXIS_ARESETN(out_m_aresetn),
		.M_AXIS_TVALID(out_m_tvalid),
		.M_AXIS_TDATA(out_m_tdata),
		.M_AXIS_TSTRB(out_m_tstrb),
		.M_AXIS_TLAST(out_m_tlast),
		.M_AXIS_TREADY(out_m_tready)
	);
*/

	// Add user logic here
	wire [20:0] out_m_tdata21;
    hamming_enc hamming_enc_i (
      .in  (in_s_tdata[15:0]  ),  // input [15:0]
      .out (out_m_tdata21     )   // output [20:0]
    ); 
    
    assign in_s_tready  = out_m_tready;
	assign out_m_tvalid = in_s_tvalid;		
	assign out_m_tdata  = {21'd0, out_m_tdata21};

	assign out_m_tstrb = in_s_tstrb;
	assign out_m_tlast = in_s_tlast;

	// User logic ends

	endmodule
