
`timescale 1 ns / 1 ps

	module noise_generator_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface ctrl
		parameter integer C_ctrl_DATA_WIDTH	= 32,
		parameter integer C_ctrl_ADDR_WIDTH	= 8,

		// Parameters of Axi Slave Bus Interface in
		parameter integer C_in_TDATA_WIDTH	= 32,

		// Parameters of Axi Master Bus Interface out
		parameter integer C_out_TDATA_WIDTH	= 32,
		parameter integer C_out_START_COUNT	= 32
	)
	(
		// Users to add ports here
		output [C_ctrl_DATA_WIDTH-1:0]	r0,
		output [C_ctrl_DATA_WIDTH-1:0]	r1,
		output [C_ctrl_DATA_WIDTH-1:0]	r2,
		output [C_ctrl_DATA_WIDTH-1:0]	r3,
		output [C_ctrl_DATA_WIDTH-1:0]	r4,
		output [C_ctrl_DATA_WIDTH-1:0]	r5,
		output [C_ctrl_DATA_WIDTH-1:0]	r6,
		output [C_ctrl_DATA_WIDTH-1:0]	r7,
		output [C_ctrl_DATA_WIDTH-1:0]	r8,
		output [C_ctrl_DATA_WIDTH-1:0]	r9,
		output [C_ctrl_DATA_WIDTH-1:0]	r10,
		output [C_ctrl_DATA_WIDTH-1:0]	r11,
		output [C_ctrl_DATA_WIDTH-1:0]	r12,
		output [C_ctrl_DATA_WIDTH-1:0]	r13,
		output [C_ctrl_DATA_WIDTH-1:0]	r14,
		output [C_ctrl_DATA_WIDTH-1:0]	r15,
		output [C_ctrl_DATA_WIDTH-1:0]	r16,
		output [C_ctrl_DATA_WIDTH-1:0]	r17,
		output [C_ctrl_DATA_WIDTH-1:0]	r18,
		output [C_ctrl_DATA_WIDTH-1:0]	r19,
		output [C_ctrl_DATA_WIDTH-1:0]	r20,
		output [C_ctrl_DATA_WIDTH-1:0]	r21,
		output [C_ctrl_DATA_WIDTH-1:0]	r22,
		output [C_ctrl_DATA_WIDTH-1:0]	r23,
		output [C_ctrl_DATA_WIDTH-1:0]	r24,
		output [C_ctrl_DATA_WIDTH-1:0]	r25,
		output [C_ctrl_DATA_WIDTH-1:0]	r26,
		output [C_ctrl_DATA_WIDTH-1:0]	r27,
		output [C_ctrl_DATA_WIDTH-1:0]	r28,
		output [C_ctrl_DATA_WIDTH-1:0]	r29,
		output [C_ctrl_DATA_WIDTH-1:0]	r30,
		output [C_ctrl_DATA_WIDTH-1:0]	r31,
		output [C_ctrl_DATA_WIDTH-1:0]	r32,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface ctrl
		input wire  ctrl_aclk,
		input wire  ctrl_aresetn,
		input wire [C_ctrl_ADDR_WIDTH-1 : 0] ctrl_awaddr,
		input wire [2 : 0] ctrl_awprot,
		input wire  ctrl_awvalid,
		output wire  ctrl_awready,
		input wire [C_ctrl_DATA_WIDTH-1 : 0] ctrl_wdata,
		input wire [(C_ctrl_DATA_WIDTH/8)-1 : 0] ctrl_wstrb,
		input wire  ctrl_wvalid,
		output wire  ctrl_wready,
		output wire [1 : 0] ctrl_bresp,
		output wire  ctrl_bvalid,
		input wire  ctrl_bready,
		input wire [C_ctrl_ADDR_WIDTH-1 : 0] ctrl_araddr,
		input wire [2 : 0] ctrl_arprot,
		input wire  ctrl_arvalid,
		output wire  ctrl_arready,
		output wire [C_ctrl_DATA_WIDTH-1 : 0] ctrl_rdata,
		output wire [1 : 0] ctrl_rresp,
		output wire  ctrl_rvalid,
		input wire  ctrl_rready,

		// Ports of Axi Slave Bus Interface in
		input wire  in_aclk,
		input wire  in_aresetn,
		output wire  in_tready,
		input wire [C_in_TDATA_WIDTH-1 : 0] in_tdata,
		input wire [(C_in_TDATA_WIDTH/8)-1 : 0] in_tstrb,
		input wire  in_tlast,
		input wire  in_tvalid,

		// Ports of Axi Master Bus Interface out
		input wire  out_aclk,
		input wire  out_aresetn,
		output wire  out_tvalid,
		output wire [C_out_TDATA_WIDTH-1 : 0] out_tdata,
		output wire [(C_out_TDATA_WIDTH/8)-1 : 0] out_tstrb,
		output wire  out_tlast,
		input wire  out_tready
	);
// Instantiation of Axi Bus Interface ctrl
	noise_generator_v1_0_ctrl # ( 
		.C_S_AXI_DATA_WIDTH(C_ctrl_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_ctrl_ADDR_WIDTH)
	) noise_generator_v1_0_ctrl_inst (
		.r0(r0),
		.r1(r1),
		.r2(r2),
		.r3(r3),
		.r4(r4),
		.r5(r5),
		.r6(r6),
		.r7(r7),
		.r8(r8),
		.r9(r9),
		.r10(r10),
		.r11(r11),
		.r12(r12),
		.r13(r13),
		.r14(r14),
		.r15(r15),
		.r16(r16),
		.r17(r17),
		.r18(r18),
		.r19(r19),
		.r20(r20),
		.r21(r21),
		.r22(r22),
		.r23(r23),
		.r24(r24),
		.r25(r25),
		.r26(r26),
		.r27(r27),
		.r28(r28),
		.r29(r29),
		.r30(r30),
		.r31(r31),
		.r32(r32),
		.S_AXI_ACLK(ctrl_aclk),
		.S_AXI_ARESETN(ctrl_aresetn),
		.S_AXI_AWADDR(ctrl_awaddr),
		.S_AXI_AWPROT(ctrl_awprot),
		.S_AXI_AWVALID(ctrl_awvalid),
		.S_AXI_AWREADY(ctrl_awready),
		.S_AXI_WDATA(ctrl_wdata),
		.S_AXI_WSTRB(ctrl_wstrb),
		.S_AXI_WVALID(ctrl_wvalid),
		.S_AXI_WREADY(ctrl_wready),
		.S_AXI_BRESP(ctrl_bresp),
		.S_AXI_BVALID(ctrl_bvalid),
		.S_AXI_BREADY(ctrl_bready),
		.S_AXI_ARADDR(ctrl_araddr),
		.S_AXI_ARPROT(ctrl_arprot),
		.S_AXI_ARVALID(ctrl_arvalid),
		.S_AXI_ARREADY(ctrl_arready),
		.S_AXI_RDATA(ctrl_rdata),
		.S_AXI_RRESP(ctrl_rresp),
		.S_AXI_RVALID(ctrl_rvalid),
		.S_AXI_RREADY(ctrl_rready)
	);

// Instantiation of Axi Bus Interface in
/*
	noise_generator_v1_0_in # ( 
		.C_S_AXIS_TDATA_WIDTH(C_in_TDATA_WIDTH)
	) noise_generator_v1_0_in_inst (
		.S_AXIS_ACLK(in_aclk),
		.S_AXIS_ARESETN(in_aresetn),
		.S_AXIS_TREADY(in_tready),
		.S_AXIS_TDATA(in_tdata),
		.S_AXIS_TSTRB(in_tstrb),
		.S_AXIS_TLAST(in_tlast),
		.S_AXIS_TVALID(in_tvalid)
	);
*/
// Instantiation of Axi Bus Interface out
/*
	noise_generator_v1_0_out # ( 
		.C_M_AXIS_TDATA_WIDTH(C_out_TDATA_WIDTH),
		.C_M_START_COUNT(C_out_START_COUNT)
	) noise_generator_v1_0_out_inst (
		.M_AXIS_ACLK(out_aclk),
		.M_AXIS_ARESETN(out_aresetn),
		.M_AXIS_TVALID(out_tvalid),
		.M_AXIS_TDATA(out_tdata),
		.M_AXIS_TSTRB(out_tstrb),
		.M_AXIS_TLAST(out_tlast),
		.M_AXIS_TREADY(out_tready)
	);
*/
	// Add user logic here
	wire [15:0][7:0] seeds;
	wire [15:0][7:0] levels;
    wire [15:0][7:0] nums;
	wire master_en, load_seeds;
	
	reg [15:0] flip_nxt, flip_ff;
	
	assign seeds[0] = r0[7:0];
	assign seeds[1] = r1[7:0];
	assign seeds[2] = r2[7:0];
	assign seeds[3] = r3[7:0];
	assign seeds[4] = r4[7:0];
	assign seeds[5] = r5[7:0];
	assign seeds[6] = r6[7:0];
	assign seeds[7] = r7[7:0];
	assign seeds[8] = r8[7:0];
	assign seeds[9] = r9[7:0];
	assign seeds[10] = r10[7:0];
	assign seeds[11] = r11[7:0];
	assign seeds[12] = r12[7:0];
	assign seeds[13] = r13[7:0];
	assign seeds[14] = r14[7:0];
	assign seeds[15] = r15[7:0];

	assign levels[0] = r16[7:0];
	assign levels[1] = r17[7:0];
	assign levels[2] = r18[7:0];
	assign levels[3] = r19[7:0];
	assign levels[4] = r20[7:0];
	assign levels[5] = r21[7:0];
	assign levels[6] = r22[7:0];
	assign levels[7] = r23[7:0];
	assign levels[8] = r24[7:0];
	assign levels[9] = r25[7:0];
	assign levels[10] = r26[7:0];
	assign levels[11] = r27[7:0];
	assign levels[12] = r28[7:0];
	assign levels[13] = r29[7:0];
	assign levels[14] = r30[7:0];
	assign levels[15] = r31[7:0];

	assign master_en  = r32[0];
	assign load_seeds = r32[1];

	genvar i;
	generate for(i=0; i<16; i=i+1) begin : gen_lfsrs
		
	lfsr_8 lfsr_8_i (
      .clk  (in_aclk    ),
      .rst  (~in_aresetn),
	  .load (load_seeds ),
      .seed (seeds[i]   ), //input  [7:0] 
      .dat  (nums[i]    )  //output [7:0] 
    );
    
    always @ (*) begin
      flip_nxt[i] = (nums[i] < levels[i])? master_en : 0;
    end
    
    always @(posedge in_aclk) begin
      if (~in_aresetn) begin
        flip_ff[i] <= 1'b0;
      end 
      else begin
        flip_ff[i] <= flip_nxt[i];
      end
    end    

    assign out_tdata[i] = /*(in_tvalid & out_tready)?*/ in_tdata[i] ^ flip_ff[i]; // : in_tdata[i]; // insert data error if requested

    end
    endgenerate
    
    assign in_tready =  out_tready;
	assign out_tvalid =  in_tvalid;		

	assign out_tstrb = in_tstrb;
	assign out_tlast = in_tlast;

	// User logic ends

	endmodule
