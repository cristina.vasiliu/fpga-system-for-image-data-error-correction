`timescale 1ns / 1ps

module tb ();
  // ctrl i/f
  parameter C_DATA_WIDTH     = 32;
  parameter C_ENC_ADDR_WIDTH =  4;
  parameter C_NSE_ADDR_WIDTH =  8;
  parameter C_DEC_ADDR_WIDTH =  4;
  // data i/f
  parameter D_DATA_WIDTH     = 32;
  parameter D_START_COUNT    = 32;

  // input data
  reg                            i_clk, i_rstn;
  wire                           i_tready;
  wire  [D_DATA_WIDTH-1 : 0]     i_tdata ;
  //wire  [(D_DATA_WIDTH/8)-1 : 0] i_tstrb ; // replaced by sof
  wire                           i_tlast ;
  wire                           i_tvalid;
  wire                           sof;

  // enc -> noise gen
  wire                           i2_tready;
  wire [D_DATA_WIDTH-1 : 0]      i2_tdata ;
  wire [(D_DATA_WIDTH/8)-1 : 0]  i2_tstrb ;
  wire                           i2_tlast ;
  wire                           i2_tvalid;

  // noise gen -> dec
  wire                           i3_tready;
  wire [D_DATA_WIDTH-1 : 0]      i3_tdata ;
  wire [(D_DATA_WIDTH/8)-1 : 0]  i3_tstrb ;
  wire                           i3_tlast ;
  wire                           i3_tvalid;

  // output data
  reg                            o_tready ;
  wire                           o_tvalid ;
  wire [D_DATA_WIDTH-1 : 0]      o_tdata  ;
  wire [(D_DATA_WIDTH/8)-1 : 0]  o_tstrb  ;
  wire                           o_tlast  ;

  reg                            c_clk, c_rstn; // control

  // enc ctrl
  reg    enc_awaddr   = 0;       // input  [C_ENC_ADDR_WIDTH-1 : 0] ENC
  reg    enc_awprot   = 0;       // input  [2 : 0]                  ENC
  reg    enc_awvalid  = 0;       // input                           ENC
  wire   enc_awready     ;       // output                          ENC
  reg    enc_wdata    = 0;       // input  [C_DATA_WIDTH-1 : 0]     ENC
  reg    enc_wstrb    = 0;       // input  [(C_DATA_WIDTH/8)-1 : 0] ENC
  reg    enc_wvalid   = 0;       // input                           ENC
  wire   enc_wready      ;       // output                          ENC
  wire   enc_bresp       ;       // output [1 : 0]                  ENC
  wire   enc_bvalid      ;       // output                          ENC
  reg    enc_araddr   = 0;       // input  [C_ENC_ADDR_WIDTH-1 : 0] ENC
  reg    enc_arprot   = 0;       // input  [2 : 0]                  ENC
  reg    enc_arvalid  = 0;       // input                           ENC
  wire   enc_arready     ;       // output                          ENC
  wire   enc_rdata       ;       // output [C_DATA_WIDTH-1 : 0]     ENC
  wire   enc_rresp       ;       // output [1 : 0]                  ENC
  wire   enc_rvalid      ;       // output                          ENC

  // nse ctrl
  reg    nse1_awaddr   = 0;       // input  [C_NSE_ADDR_WIDTH-1 : 0] NSE1
  reg    nse1_awprot   = 0;       // input  [2 : 0]                  NSE1
  reg    nse1_awvalid  = 0;       // input                           NSE1
  wire   nse1_awready     ;       // output                          NSE1
  reg    nse1_wdata    = 0;       // input  [C_DATA_WIDTH-1 : 0]     NSE1
  reg    nse1_wstrb    = 0;       // input  [(C_DATA_WIDTH/8)-1 : 0] NSE1
  reg    nse1_wvalid   = 0;       // input                           NSE1
  wire   nse1_wready      ;       // output                          NSE1
  wire   nse1_bresp       ;       // output [1 : 0]                  NSE1
  wire   nse1_bvalid      ;       // output                          NSE1
  reg    nse1_araddr   = 0;       // input  [C_NSE_ADDR_WIDTH-1 : 0] NSE1
  reg    nse1_arprot   = 0;       // input  [2 : 0]                  NSE1
  reg    nse1_arvalid  = 0;       // input                           NSE1
  wire   nse1_arready     ;       // output                          NSE1
  wire   nse1_rdata       ;       // output [C_DATA_WIDTH-1 : 0]     NSE1
  wire   nse1_rresp       ;       // output [1 : 0]                  NSE1
  wire   nse1_rvalid      ;       // output                          NSE1 

  reg    nse0_awaddr   = 0;       // input  [C_NSE_ADDR_WIDTH-1 : 0] NSE0
  reg    nse0_awprot   = 0;       // input  [2 : 0]                  NSE0
  reg    nse0_awvalid  = 0;       // input                           NSE0
  wire   nse0_awready     ;       // output                          NSE0
  reg    nse0_wdata    = 0;       // input  [C_DATA_WIDTH-1 : 0]     NSE0
  reg    nse0_wstrb    = 0;       // input  [(C_DATA_WIDTH/8)-1 : 0] NSE0
  reg    nse0_wvalid   = 0;       // input                           NSE0
  wire   nse0_wready      ;       // output                          NSE0
  wire   nse0_bresp       ;       // output [1 : 0]                  NSE0
  wire   nse0_bvalid      ;       // output                          NSE0
  reg    nse0_araddr   = 0;       // input  [C_NSE_ADDR_WIDTH-1 : 0] NSE0
  reg    nse0_arprot   = 0;       // input  [2 : 0]                  NSE0
  reg    nse0_arvalid  = 0;       // input                           NSE0
  wire   nse0_arready     ;       // output                          NSE0
  wire   nse0_rdata       ;       // output [C_DATA_WIDTH-1 : 0]     NSE0
  wire   nse0_rresp       ;       // output [1 : 0]                  NSE0
  wire   nse0_rvalid      ;       // output                          NSE0

  // dec ctrl
  reg    dec_awaddr   = 0;       // input  [C_DEC_ADDR_WIDTH-1 : 0] DEC
  reg    dec_awprot   = 0;       // input  [2 : 0]                  DEC
  reg    dec_awvalid  = 0;       // input                           DEC
  wire   dec_awready     ;       // output                          DEC
  reg    dec_wdata    = 0;       // input  [C_DATA_WIDTH-1 : 0]     DEC
  reg    dec_wstrb    = 0;       // input  [(C_DATA_WIDTH/8)-1 : 0] DEC
  reg    dec_wvalid   = 0;       // input                           DEC
  wire   dec_wready      ;       // output                          DEC
  wire   dec_bresp       ;       // output [1 : 0]                  DEC
  wire   dec_bvalid      ;       // output                          DEC
  reg    dec_araddr   = 0;       // input  [C_DEC_ADDR_WIDTH-1 : 0] DEC
  reg    dec_arprot   = 0;       // input  [2 : 0]                  DEC
  reg    dec_arvalid  = 0;       // input                           DEC
  wire   dec_arready     ;       // output                          DEC
  wire   dec_rdata       ;       // output [C_DATA_WIDTH-1 : 0]     DEC
  wire   dec_rresp       ;       // output [1 : 0]                  DEC
  wire   dec_rvalid      ;       // output                          DEC


  wire                           errored_det;


  frame_gen #(
    .DW    (C_DATA_WIDTH)
  ) frame_gen_i (
    .clk   (i_clk       ), // input
    .rstn  (i_rstn      ), // input
    .ready (i_tready    ), // input
    .dat   (i_tdata     ), // output [31:0]
    .sof   (sof         ), // output [ 3:0]
    .eol   (i_tlast     ), // output
    .val   (i_tvalid    )  // output

  );


  encoder_hamming_v1_0  #(
    .C_ctrl_s_DATA_WIDTH (C_DATA_WIDTH    ),
    .C_ctrl_s_ADDR_WIDTH (C_ENC_ADDR_WIDTH),
    .C_in_s_TDATA_WIDTH  (D_DATA_WIDTH    ),
    .C_out_m_TDATA_WIDTH (D_DATA_WIDTH    ),
    .C_out_m_START_COUNT (D_START_COUNT   )
  ) encoder_hamming_i (
    .r0             (              ), // output [C_ctrl_s_DATA_WIDTH-1:0]
    .r1             (              ), // output [C_ctrl_s_DATA_WIDTH-1:0]
    .r2             (              ), // output [C_ctrl_s_DATA_WIDTH-1:0]
    .r3             (              ), // output [C_ctrl_s_DATA_WIDTH-1:0]

    .ctrl_s_aclk    (c_clk         ), // input
    .ctrl_s_aresetn (c_rstn        ), // input
    .ctrl_s_awaddr  (enc_awaddr    ), // input  [C_ctrl_s_ADDR_WIDTH-1 : 0]
    .ctrl_s_awprot  (enc_awprot    ), // input  [2 : 0]
    .ctrl_s_awvalid (enc_awvalid   ), // input
    .ctrl_s_awready (enc_awready   ), // output

    .ctrl_s_wdata   (enc_wdata     ), // input  [C_ctrl_s_DATA_WIDTH-1 : 0]
    .ctrl_s_wstrb   (enc_wstrb     ), // input  [(C_ctrl_s_DATA_WIDTH/8)-1 : 0]
    .ctrl_s_wvalid  (enc_wvalid    ), // input
    .ctrl_s_wready  (enc_wready    ), // output

    .ctrl_s_bresp   (enc_bresp     ), // output [1 : 0]
    .ctrl_s_bvalid  (enc_bvalid    ), // output
    .ctrl_s_bready  (1'b1          ), // input

    .ctrl_s_araddr  (enc_araddr    ), // input  [C_ctrl_s_ADDR_WIDTH-1 : 0]
    .ctrl_s_arprot  (enc_arprot    ), // input  [2 : 0]
    .ctrl_s_arvalid (enc_arvalid   ), // input
    .ctrl_s_arready (enc_arready   ), // output

    .ctrl_s_rdata   (enc_rdata     ), // output [C_ctrl_s_DATA_WIDTH-1 : 0]
    .ctrl_s_rresp   (enc_rresp     ), // output [1 : 0]
    .ctrl_s_rvalid  (enc_rvalid    ), // output
    .ctrl_s_rready  (1'b1          ), // input

    .in_s_aclk      (i_clk         ), // input
    .in_s_aresetn   (i_rstn        ), // input
    .in_s_tready    (i_tready      ), // output
    .in_s_tdata     (i_tdata       ), // input  [C_in_s_TDATA_WIDTH-1 : 0]
    .in_s_tstrb     ({3'h0,sof}    ), // input  [(C_in_s_TDATA_WIDTH/8)-1 : 0]
    .in_s_tlast     (i_tlast       ), // input
    .in_s_tvalid    (i_tvalid      ), // input

    .out_m_aclk     (i_clk         ), // input
    .out_m_aresetn  (i_rstn        ), // input
    .out_m_tvalid   (i2_tvalid     ), // output
    .out_m_tdata    (i2_tdata      ), // output [C_out_m_TDATA_WIDTH-1 : 0]
    .out_m_tstrb    (i2_tstrb      ), // output [(C_out_m_TDATA_WIDTH/8)-1 : 0]
    .out_m_tlast    (i2_tlast      ), // output
    .out_m_tready   (i2_tready     )  // input
  );

  noise_generator_v1_0 #(
    .C_ctrl_DATA_WIDTH (C_DATA_WIDTH    ),
    .C_ctrl_ADDR_WIDTH (C_NSE_ADDR_WIDTH),
    .C_in_TDATA_WIDTH  (D_DATA_WIDTH    ),
    .C_out_TDATA_WIDTH (D_DATA_WIDTH    ),
    .C_out_START_COUNT (D_START_COUNT   )
  ) noise_generator_1 (
    .r0             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r1             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r2             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r3             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r4             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r5             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r6             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r7             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r8             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r9             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r10            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r11            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r12            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r13            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r14            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r15            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r16            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r17            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r18            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r19            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r20            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r21            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r22            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r23            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r24            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r25            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r26            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r27            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r28            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r29            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r30            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r31            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r32            (              ), // output [C_ctrl_DATA_WIDTH-1:0]

    .ctrl_aclk      (c_clk         ), // input
    .ctrl_aresetn   (c_rstn        ), // input
    .ctrl_awaddr    (nse0_awaddr   ), // input  [C_ctrl_ADDR_WIDTH-1 : 0]
    .ctrl_awprot    (nse0_awprot   ), // input  [2 : 0]
    .ctrl_awvalid   (nse0_awvalid  ), // input
    .ctrl_awready   (nse0_awready  ), // output

    .ctrl_wdata     (nse0_wdata    ), // input  [C_ctrl_DATA_WIDTH-1 : 0]
    .ctrl_wstrb     (nse0_wstrb    ), // input  [(C_ctrl_DATA_WIDTH/8)-1 : 0]
    .ctrl_wvalid    (nse0_wvalid   ), // input
    .ctrl_wready    (nse0_wready   ), // output

    .ctrl_bresp     (nse0_bresp    ), // output [1 : 0]
    .ctrl_bvalid    (nse0_bvalid   ), // output
    .ctrl_bready    (1'b1          ), // input

    .ctrl_araddr    (nse0_araddr   ), // input  [C_ctrl_ADDR_WIDTH-1 : 0]
    .ctrl_arprot    (nse0_arprot   ), // input  [2 : 0]
    .ctrl_arvalid   (nse0_arvalid  ), // input
    .ctrl_arready   (nse0_arready  ), // output

    .ctrl_rdata     (nse0_rdata    ), // output [C_ctrl_DATA_WIDTH-1 : 0]
    .ctrl_rresp     (nse0_rresp    ), // output [1 : 0]
    .ctrl_rvalid    (nse0_rvalid   ), // output
    .ctrl_rready    (1'b1          ), // input

    .in_aclk        (i_clk         ), // input
    .in_aresetn     (i_rstn        ), // input
    .in_tready      (              ), // output
    .in_tdata       (i2_tdata[31:16]), // input  [C_in_TDATA_WIDTH-1 : 0]
    .in_tstrb       (i2_tstrb      ), // input  [(C_in_TDATA_WIDTH/8)-1 : 0]
    .in_tlast       (i2_tlast      ), // input
    .in_tvalid      (i2_tvalid     ), // input

    .out_aclk       (i_clk         ), // input
    .out_aresetn    (i_rstn        ), // input
    .out_tvalid     (              ), // output
    .out_tdata      (i3_tdata[31:16]), // output [C_out_TDATA_WIDTH-1 : 0]
    .out_tstrb      (              ), // output [(C_out_TDATA_WIDTH/8)-1 : 0]
    .out_tlast      (              ), // output
    .out_tready     (i3_tready     )  // input
 );

  noise_generator_v1_0 #(
    .C_ctrl_DATA_WIDTH (C_DATA_WIDTH    ),
    .C_ctrl_ADDR_WIDTH (C_NSE_ADDR_WIDTH),
    .C_in_TDATA_WIDTH  (D_DATA_WIDTH    ),
    .C_out_TDATA_WIDTH (D_DATA_WIDTH    ),
    .C_out_START_COUNT (D_START_COUNT   )
  ) noise_generator_0 (
    .r0             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r1             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r2             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r3             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r4             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r5             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r6             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r7             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r8             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r9             (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r10            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r11            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r12            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r13            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r14            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r15            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r16            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r17            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r18            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r19            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r20            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r21            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r22            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r23            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r24            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r25            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r26            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r27            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r28            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r29            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r30            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r31            (              ), // output [C_ctrl_DATA_WIDTH-1:0]
    .r32            (              ), // output [C_ctrl_DATA_WIDTH-1:0]

    .ctrl_aclk      (c_clk         ), // input
    .ctrl_aresetn   (c_rstn        ), // input
    .ctrl_awaddr    (nse1_awaddr   ), // input  [C_ctrl_ADDR_WIDTH-1 : 0]
    .ctrl_awprot    (nse1_awprot   ), // input  [2 : 0]
    .ctrl_awvalid   (nse1_awvalid  ), // input
    .ctrl_awready   (nse1_awready  ), // output

    .ctrl_wdata     (nse1_wdata    ), // input  [C_ctrl_DATA_WIDTH-1 : 0]
    .ctrl_wstrb     (nse1_wstrb    ), // input  [(C_ctrl_DATA_WIDTH/8)-1 : 0]
    .ctrl_wvalid    (nse1_wvalid   ), // input
    .ctrl_wready    (nse1_wready   ), // output

    .ctrl_bresp     (nse1_bresp    ), // output [1 : 0]
    .ctrl_bvalid    (nse1_bvalid   ), // output
    .ctrl_bready    (1'b1          ), // input

    .ctrl_araddr    (nse1_araddr   ), // input  [C_ctrl_ADDR_WIDTH-1 : 0]
    .ctrl_arprot    (nse1_arprot   ), // input  [2 : 0]
    .ctrl_arvalid   (nse1_arvalid  ), // input
    .ctrl_arready   (nse1_arready  ), // output

    .ctrl_rdata     (nse1_rdata    ), // output [C_ctrl_DATA_WIDTH-1 : 0]
    .ctrl_rresp     (nse1_rresp    ), // output [1 : 0]
    .ctrl_rvalid    (nse1_rvalid   ), // output
    .ctrl_rready    (1'b1          ), // input

    .in_aclk        (i_clk         ), // input
    .in_aresetn     (i_rstn        ), // input
    .in_tready      (i2_tready     ), // output
    .in_tdata       (i2_tdata[15:0]), // input  [C_in_TDATA_WIDTH-1 : 0]
    .in_tstrb       (i2_tstrb      ), // input  [(C_in_TDATA_WIDTH/8)-1 : 0]
    .in_tlast       (i2_tlast      ), // input
    .in_tvalid      (i2_tvalid     ), // input

    .out_aclk       (i_clk         ), // input
    .out_aresetn    (i_rstn        ), // input
    .out_tvalid     (i3_tvalid     ), // output
    .out_tdata      (i3_tdata[15:0]), // output [C_out_TDATA_WIDTH-1 : 0]
    .out_tstrb      (i3_tstrb      ), // output [(C_out_TDATA_WIDTH/8)-1 : 0]
    .out_tlast      (i3_tlast      ), // output
    .out_tready     (i3_tready     )  // input
 );


 decoder_hamming_v1_0  #
        (
    .C_ctrl_s_DATA_WIDTH (C_DATA_WIDTH    ),
    .C_ctrl_s_ADDR_WIDTH (C_DEC_ADDR_WIDTH),
    .C_in_s_TDATA_WIDTH  (D_DATA_WIDTH    ),
    .C_out_m_TDATA_WIDTH (D_DATA_WIDTH    ),
    .C_out_m_START_COUNT (D_START_COUNT   )
 ) decoder_hamming_i (
    .r0             (              ), // output [C_ctrl_s_DATA_WIDTH-1:0]
    .r1             (              ), // output [C_ctrl_s_DATA_WIDTH-1:0]
    .r2             (              ), // output [C_ctrl_s_DATA_WIDTH-1:0]
    .r3             (              ), // output [C_ctrl_s_DATA_WIDTH-1:0]
    .errored_det    (errored_det   ), // output

    .ctrl_s_aclk    (c_clk      ), // input
    .ctrl_s_aresetn (c_rstn     ), // input

    .ctrl_s_awaddr  (dec_awaddr ), // input  [C_ctrl_s_ADDR_WIDTH-1 : 0]
    .ctrl_s_awprot  (dec_awprot ), // input  [2 : 0]
    .ctrl_s_awvalid (dec_awvalid), // input
    .ctrl_s_awready (dec_awready), // output

    .ctrl_s_wdata   (dec_wdata  ), // input  [C_ctrl_s_DATA_WIDTH-1 : 0]
    .ctrl_s_wstrb   (dec_wstrb  ), // input  [(C_ctrl_s_DATA_WIDTH/8)-1 : 0]
    .ctrl_s_wvalid  (dec_wvalid ), // input
    .ctrl_s_wready  (dec_wready ), // output

    .ctrl_s_bresp   (dec_bresp  ), // output [1 : 0]
    .ctrl_s_bvalid  (dec_bvalid ), // output
    .ctrl_s_bready  (1'b1       ), // input

    .ctrl_s_araddr  (dec_araddr ), // input  [C_ctrl_s_ADDR_WIDTH-1 : 0]
    .ctrl_s_arprot  (dec_arprot ), // input  [2 : 0]
    .ctrl_s_arvalid (dec_arvalid), // input
    .ctrl_s_arready (dec_arready), // output

    .ctrl_s_rdata   (dec_rdata  ), // output [C_ctrl_s_DATA_WIDTH-1 : 0]
    .ctrl_s_rresp   (dec_rresp  ), // output [1 : 0]
    .ctrl_s_rvalid  (dec_rvalid ), // output
    .ctrl_s_rready  (1'b1       ), // input

    .in_s_aclk      (i_clk         ), // input
    .in_s_aresetn   (i_rstn        ), // input
    .in_s_tready    (i3_tready     ), // output
    .in_s_tdata     (i3_tdata      ), // input  [C_in_s_TDATA_WIDTH-1 : 0]
    .in_s_tstrb     (i3_tstrb      ), // input  [(C_in_s_TDATA_WIDTH/8)-1 : 0]
    .in_s_tlast     (i3_tlast      ), // input
    .in_s_tvalid    (i3_tvalid     ), // input

    .out_m_aclk     (i_clk         ), // input
    .out_m_aresetn  (i_rstn        ), // input
    .out_m_tvalid   (o_tvalid      ), // output
    .out_m_tdata    (o_tdata       ), // output [C_out_m_TDATA_WIDTH-1 : 0]
    .out_m_tstrb    (o_tstrb       ), // output [(C_out_m_TDATA_WIDTH/8)-1 : 0]
    .out_m_tlast    (o_tlast       ), // output
    .out_m_tready   (o_tready      )  // input
  );


  // data
  always begin
    #3 i_clk = ~i_clk ; // 166Mhz
  end

  always @(posedge i_clk) begin
    o_tready <= ($urandom%4 == 0)? 1'b0 : 1'b1 ;
  end

  initial begin
    i_clk    = 1'b0;
    i_rstn   = 1'b0;  repeat (5) @(posedge i_clk);  i_rstn   = 1'b1;
    fork
      begin
          repeat (5000) @(posedge i_clk);
          $stop;        
      end
      repeat (5000) begin // insert errors
        repeat ($random%5) @(posedge i_clk);
        force tb.noise_generator_0.flip_ff[5] = 1;
        @(posedge i_clk); release tb.noise_generator_0.flip_ff[5];
      end

      repeat (500) begin // insert errors
        repeat ($random%50) @(posedge i_clk);
        force tb.decoder_hamming_i.r0[0] = 1;
        repeat (10) @(posedge i_clk); release tb.decoder_hamming_i.r0[0];
      end
    join
  end

  // ctrl
  always begin
    #4 c_clk = ~c_clk ; // 125Mhz
  end

  initial begin
    c_clk    = 1'b0;
    c_rstn   = 1'b0;  repeat (5) @(posedge c_clk);  c_rstn   = 1'b1;
  end


/*
  initial begin
    $display("*************************************************  START OF TESTBENCH ****************************************************");
    $monitor(" input data = %b, output data = %b, errored bit = %b (1=yes, 0=no)", in, out, errored_bit);

    #1 $display(" 1 bit error on bit 3 (2) - error on data bit:");
    in = 21'b0_1101_1010_0111_0010_1001;

    #1 $display(" 0 bit error:");
    in = 21'b1_0110_1111_1010_1010_0111;

    #1 $display(" 1 bit error on bit 8 (7) - error on parity bit:");
    in = 21'b1_0110_1111_1010_0010_0111;

    #1 $display(" 1 bit error on bit 19 (18) - error on data bit:");
    in = 21'b1_0010_1111_1010_1010_0111;
    #1
    $display("*************************************************  END OF TESTBENCH  *****************************************************");
  end
*/

endmodule
