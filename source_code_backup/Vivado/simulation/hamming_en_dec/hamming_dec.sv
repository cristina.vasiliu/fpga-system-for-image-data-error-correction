`timescale 1ns / 1ps

module hamming_dec(
  input  [20:0] in,
  output [15:0] out,
  output        errored_bit
  );
        
  reg p0, p1, p3, p7, p15;
  // corrected data = error syndrome ^ recieved data (in) 
  reg  [20:0] error_syndrome;
  wire [20:0] corrected_data;

  always @ (*) begin
    p0  = in[0]  ^ in[2]  ^ in[4]  ^ in[6]  ^ in[8]  ^ in[10] ^ in[12] ^ in[14] ^ in[16] ^ in[18] ^ in[20];
    p1  = in[1]  ^ in[2]  ^ in[5]  ^ in[6]  ^ in[9]  ^ in[10] ^ in[13] ^ in[14] ^ in[17] ^ in[18];
    p3  = in[3]  ^ in[4]  ^ in[5]  ^ in[6]  ^ in[11] ^ in[12] ^ in[13] ^ in[14] ^ in[19] ^ in[20];
    p7  = in[7]  ^ in[8]  ^ in[9]  ^ in[10] ^ in[11] ^ in[12] ^ in[13] ^ in[14];
    p15 = in[15] ^ in[16] ^ in[17] ^ in[18] ^ in[19] ^ in[20];
  end

  always @ (*) begin
    error_syndrome = 0;
    case ({p15, p7, p3, p1, p0})
      5'd1  : error_syndrome = 21'b0_0000_0000_0000_0000_0001;
      5'd2  : error_syndrome = 21'b0_0000_0000_0000_0000_0010;
      5'd3  : error_syndrome = 21'b0_0000_0000_0000_0000_0100;
      5'd4  : error_syndrome = 21'b0_0000_0000_0000_0000_1000;
      5'd5  : error_syndrome = 21'b0_0000_0000_0000_0001_0000;
      5'd6  : error_syndrome = 21'b0_0000_0000_0000_0010_0000;
      5'd7  : error_syndrome = 21'b0_0000_0000_0000_0100_0000;
      5'd8  : error_syndrome = 21'b0_0000_0000_0000_1000_0000;
      5'd9  : error_syndrome = 21'b0_0000_0000_0001_0000_0000;
      5'd10 : error_syndrome = 21'b0_0000_0000_0010_0000_0000;
      5'd11 : error_syndrome = 21'b0_0000_0000_0100_0000_0000;
      5'd12 : error_syndrome = 21'b0_0000_0000_1000_0000_0000;
      5'd13 : error_syndrome = 21'b0_0000_0001_0000_0000_0000;
      5'd14 : error_syndrome = 21'b0_0000_0010_0000_0000_0000;
      5'd15 : error_syndrome = 21'b0_0000_0100_0000_0000_0000;
      5'd16 : error_syndrome = 21'b0_0000_1000_0000_0000_0000;
      5'd17 : error_syndrome = 21'b0_0001_0000_0000_0000_0000;
      5'd18 : error_syndrome = 21'b0_0010_0000_0000_0000_0000;
      5'd19 : error_syndrome = 21'b0_0100_0000_0000_0000_0000;
      5'd20 : error_syndrome = 21'b0_1000_0000_0000_0000_0000;
      5'd21 : error_syndrome = 21'b1_0000_0000_0000_0000_0000;
      default : error_syndrome = 21'b0;
    endcase
  end

  assign corrected_data = error_syndrome ^ in;
 
  assign errored_bit = ( |{p15, p7, p3, p1, p0} ); // an output of 1 means that there is an error

  assign out = {corrected_data[20:16], corrected_data[14:8], corrected_data[6:4], corrected_data[2]};

endmodule

