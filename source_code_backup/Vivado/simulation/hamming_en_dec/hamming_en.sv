`timescale 1ns / 1ps

module hamming_en(
  input  [15:0] in,
  output [20:0] out);
        
  wire p0, p1, p3, p7, p15; //calculate parity bits
    
  assign p0  = in[0]  ^ in[1]  ^ in[3]  ^ in[4]  ^ in[6] ^ in[8] ^ in[10] ^ in[11] ^ in[13] ^ in[15];
  assign p1  = in[0]  ^ in[2]  ^ in[3]  ^ in[5]  ^ in[6] ^ in[9] ^ in[10] ^ in[12] ^ in[13];
  assign p3  = in[1]  ^ in[2]  ^ in[3]  ^ in[7]  ^ in[8] ^ in[9] ^ in[10] ^ in[14] ^ in[15];
  assign p7  = in[4]  ^ in[5]  ^ in[6]  ^ in[7]  ^ in[8] ^ in[9] ^ in[10];
  assign p15 = in[11] ^ in[12] ^ in[13] ^ in[14] ^ in[15];

  assign out = {in[15:11], p15, in[10:4], p7, in[3:1], p3, in[0], p1, p0};
endmodule