`timescale 1ns / 1ps

module hamming_dec_tb ();

  reg  [20:0] in;
  wire [15:0] out;
  wire        errored_bit;

  hamming_dec hamming_dec_i (
    .in          (in          ),
    .out         (out         ),
    .errored_bit (errored_bit )
  );

  initial begin 
    $display("*************************************************  START OF TESTBENCH ****************************************************");
    $monitor(" input data = %b, output data = %b, errored bit = %b (1=yes, 0=no)", in, out, errored_bit);

    #1 $display(" 1 bit error on bit 3 (2) - error on data bit:");
    in = 21'b0_1101_1010_0111_0010_1001;

    #1 $display(" 0 bit error:");
    in = 21'b1_0110_1111_1010_1010_0111;

    #1 $display(" 1 bit error on bit 8 (7) - error on parity bit:");
    in = 21'b1_0110_1111_1010_0010_0111;

    #1 $display(" 1 bit error on bit 19 (18) - error on data bit:");
    in = 21'b1_0010_1111_1010_1010_0111;
    #1
    $display("*************************************************  END OF TESTBENCH  *****************************************************");
  end  

endmodule
