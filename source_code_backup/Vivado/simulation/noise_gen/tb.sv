`timescale 1ns/1ns

/*
module tb ();

reg clk, rst;
reg [7:0] seed;
wire [7:0] num;

lfsr_8 lfsr_8_i (
      .clk  (clk    ),
      .rst  (rst    ),
      .seed (seed   ), //input  [7:0] 
      .out  (num    )  //output [7:0] 
    );

always begin
    #2;
    clk = ~ clk;
end

initial begin
    clk = 0;
    rst = 1;
    seed = 54;
    
    repeat (5) @ (posedge clk);
    rst = 0 ;
    repeat (128) @ (posedge clk);
end

endmodule */


module tb ();

reg clk, rst;
reg [7:0] seed;
wire [7:0] num;

reg [32-1:0]	r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18,r19,r20,r21,r22,r23,r24,r25,r26,r27,r28,r29,r30,r31,r32;
reg [16-1 : 0] in_tdata;

wire  out_tvalid;
wire [16-1 : 0] out_tdata;

wire flipped = (in_tdata!=out_tdata)? 1 : 0;

top top_i (

.r0 (r0 ),
.r1 (r1 ),
.r2 (r2 ),
.r3 (r3 ),
.r4 (r4 ),
.r5 (r5 ),
.r6 (r6 ),
.r7 (r7 ),
.r8 (r8 ),
.r9 (r9 ),
.r10(r10),
.r11(r11),
.r12(r12),
.r13(r13),
.r14(r14),
.r15(r15),
.r16(r16),
.r17(r17),
.r18(r18),
.r19(r19),
.r20(r20),
.r21(r21),
.r22(r22),
.r23(r23),
.r24(r24),
.r25(r25),
.r26(r26),
.r27(r27),
.r28(r28),
.r29(r29),
.r30(r30),
.r31(r31),
.r32(r32),
.in_aclk(clk),
.in_aresetn(~rst),
.in_tready(),
.in_tdata(in_tdata),
.in_tstrb(0),
.in_tlast(0),
.in_tvalid(1),
.out_aclk(clk),
.out_aresetn(~rst),
.out_tvalid(out_tvalid),
.out_tdata(out_tdata),
.out_tstrb(),
.out_tlast(),
.out_tready(1)

);


always begin
    #2;
    clk = ~ clk;
end

initial begin
    clk = 0;
    rst = 1;
    seed = 54;
    r0  = 0;
    r1  = 0;
    r2  = 0;
    r3  = 0;
    r4  = 0;
    r5  = 0;
    r6  = 0;
    r7  = 0;
    r8  = 0;
    r9  = 0;
    r10 = 0;
    r11 = 0;
    r12 = 0;
    r13 = 0;
    r14 = 0;
    r15 = 0;
    r16 = 0;
    r17 = 0;
    r18 = 0;
    r19 = 0;
    r20 = 0;
    r21 = 0;
    r22 = 0;
    r23 = 0;
    r24 = 0;
    r25 = 0;
    r26 = 0;
    r27 = 0;
    r28 = 0;
    r29 = 0;
    r30 = 0;
    r31 = 0;
    r32 = 0; 
    
    repeat (5) @ (posedge clk);
    rst = 0 ;

    fork

    repeat (4096) begin
      in_tdata = $urandom();
      @(posedge clk);
    end

    begin
      repeat (128) @ (posedge clk);
        r0  = 16;
        r1  = 1;
        r2  = 2;
        r3  = 3;
        r4  = 4;
        r5  = 5;
        r6  = 6;
        r7  = 7;
        r8  = 8;
        r9  = 9;
        r10 = 10;
        r11 = 11;
        r12 = 12;
        r13 = 13;
        r14 = 14;
        r15 = 15;
        r16 = 255;
        r17 = 255;
        r18 = 255;
        r19 = 255;
        r20 = 128; //
        r21 = 252; //
        r22 = 128;
        r23 = 255;
        r24 = 244;
        r25 = 128; //
        r26 = 200; //
        r27 = 233; //
        r28 = 150;
        r29 = 160;
        r30 = 180;
        r31 = 200;

        r32 = 1; 
        rst = 1;
        repeat (5) @ (posedge clk);
        rst = 0;


    end

    join
    repeat (5) @ (posedge clk);


end

endmodule