`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.05.2024 16:27:43
// Design Name: 
// Module Name: lfsr_8
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module lfsr_8 (
  input clk,
  input rst,
  input      [7:0] seed,
  output reg [7:0] dat );

  always @(posedge clk) begin
    if (rst) begin
      dat <= seed;
    end 
    else begin
      dat <= {dat[6:0], dat[7] ^ dat[6]}; // Feedback polynomial x^8 + x^6 + x^5 + x^4 + 1
    end
  end
endmodule
