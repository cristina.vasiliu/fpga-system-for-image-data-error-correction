
`timescale 1 ns / 1 ps

	module top #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface ctrl
		parameter integer C_ctrl_DATA_WIDTH	= 32,
		parameter integer C_ctrl_ADDR_WIDTH	= 8,

		// Parameters of Axi Slave Bus Interface in
		parameter integer C_in_TDATA_WIDTH	= 32,

		// Parameters of Axi Master Bus Interface out
		parameter integer C_out_TDATA_WIDTH	= 32,
		parameter integer C_out_START_COUNT	= 32
	)
	(
		// Users to add ports here
		input [C_ctrl_DATA_WIDTH-1:0]	r0,
		input [C_ctrl_DATA_WIDTH-1:0]	r1,
		input [C_ctrl_DATA_WIDTH-1:0]	r2,
		input [C_ctrl_DATA_WIDTH-1:0]	r3,
		input [C_ctrl_DATA_WIDTH-1:0]	r4,
		input [C_ctrl_DATA_WIDTH-1:0]	r5,
		input [C_ctrl_DATA_WIDTH-1:0]	r6,
		input [C_ctrl_DATA_WIDTH-1:0]	r7,
		input [C_ctrl_DATA_WIDTH-1:0]	r8,
		input [C_ctrl_DATA_WIDTH-1:0]	r9,
		input [C_ctrl_DATA_WIDTH-1:0]	r10,
		input [C_ctrl_DATA_WIDTH-1:0]	r11,
		input [C_ctrl_DATA_WIDTH-1:0]	r12,
		input [C_ctrl_DATA_WIDTH-1:0]	r13,
		input [C_ctrl_DATA_WIDTH-1:0]	r14,
		input [C_ctrl_DATA_WIDTH-1:0]	r15,
		input [C_ctrl_DATA_WIDTH-1:0]	r16,
		input [C_ctrl_DATA_WIDTH-1:0]	r17,
		input [C_ctrl_DATA_WIDTH-1:0]	r18,
		input [C_ctrl_DATA_WIDTH-1:0]	r19,
		input [C_ctrl_DATA_WIDTH-1:0]	r20,
		input [C_ctrl_DATA_WIDTH-1:0]	r21,
		input [C_ctrl_DATA_WIDTH-1:0]	r22,
		input [C_ctrl_DATA_WIDTH-1:0]	r23,
		input [C_ctrl_DATA_WIDTH-1:0]	r24,
		input [C_ctrl_DATA_WIDTH-1:0]	r25,
		input [C_ctrl_DATA_WIDTH-1:0]	r26,
		input [C_ctrl_DATA_WIDTH-1:0]	r27,
		input [C_ctrl_DATA_WIDTH-1:0]	r28,
		input [C_ctrl_DATA_WIDTH-1:0]	r29,
		input [C_ctrl_DATA_WIDTH-1:0]	r30,
		input [C_ctrl_DATA_WIDTH-1:0]	r31,
		input [C_ctrl_DATA_WIDTH-1:0]	r32,
		// User ports ends
		// Do not modify the ports beyond this line



		// Ports of Axi Slave Bus Interface in
		input wire  in_aclk,
		input wire  in_aresetn,
		output wire  in_tready,
		input wire [C_in_TDATA_WIDTH-1 : 0] in_tdata,
		input wire [(C_in_TDATA_WIDTH/8)-1 : 0] in_tstrb,
		input wire  in_tlast,
		input wire  in_tvalid,

		// Ports of Axi Master Bus Interface out
		input wire  out_aclk,
		input wire  out_aresetn,
		output wire  out_tvalid,
		output wire [C_out_TDATA_WIDTH-1 : 0] out_tdata,
		output wire [(C_out_TDATA_WIDTH/8)-1 : 0] out_tstrb,
		output wire  out_tlast,
		input wire  out_tready
	);

	// Add user logic here
	wire [15:0][7:0] seeds;
	wire [15:0][7:0] levels;
    wire [15:0][7:0] nums;
	wire master_en;
	
	reg [15:0] flip_nxt, flip_ff;
	
	assign seeds[0] = r0[7:0];
	assign seeds[1] = r1[7:0];
	assign seeds[2] = r2[7:0];
	assign seeds[3] = r3[7:0];
	assign seeds[4] = r4[7:0];
	assign seeds[5] = r5[7:0];
	assign seeds[6] = r6[7:0];
	assign seeds[7] = r7[7:0];
	assign seeds[8] = r8[7:0];
	assign seeds[9] = r9[7:0];
	assign seeds[10] = r10[7:0];
	assign seeds[11] = r11[7:0];
	assign seeds[12] = r12[7:0];
	assign seeds[13] = r13[7:0];
	assign seeds[14] = r14[7:0];
	assign seeds[15] = r15[7:0];

	assign levels[0] = r16[7:0];
	assign levels[1] = r17[7:0];
	assign levels[2] = r18[7:0];
	assign levels[3] = r19[7:0];
	assign levels[4] = r20[7:0];
	assign levels[5] = r21[7:0];
	assign levels[6] = r22[7:0];
	assign levels[7] = r23[7:0];
	assign levels[8] = r24[7:0];
	assign levels[9] = r25[7:0];
	assign levels[10] = r26[7:0];
	assign levels[11] = r27[7:0];
	assign levels[12] = r28[7:0];
	assign levels[13] = r29[7:0];
	assign levels[14] = r30[7:0];
	assign levels[15] = r31[7:0];

	assign master_en = r32[0];
	
	genvar i;
	generate for(i=0; i<16; i=i+1) begin : gen_lfsrs
		
	lfsr_8 lfsr_8_i (
      .clk  (in_aclk    ),
      .rst  (~in_aresetn),
      .seed (seeds[i]   ), //input  [7:0] 
      .dat  (nums[i]    )  //output [7:0] 
    );
    
    always @ (*) begin
      flip_nxt[i] = (nums[i] < levels[i])? master_en : 0;
    end
    
    always @(posedge in_aclk) begin
      if (~in_aresetn) begin
        flip_ff[i] <= 1'b0;
      end 
      else begin
        flip_ff[i] <= flip_nxt[i];
      end
    end    

    assign out_tdata[i] = (in_tvalid & out_tready)? in_tdata[i] ^ flip_ff[i] : in_tdata[i]; // insert data error if requested

    end
    endgenerate
    
    assign in_tready =  out_tready;
	assign out_tvalid =  in_tvalid;		

	assign out_tstrb = in_tstrb;
	assign out_tlast = in_tlast;

	// User logic ends

	endmodule
