
/* ------------------------------------------------------------ */
/*				Include File Definitions						*/
/* ------------------------------------------------------------ */

#include "display_demo.h"
#include "display_ctrl.h"
#include <stdio.h>
#include "math.h"
#include <ctype.h>
#include <stdlib.h>
#include "xil_types.h"
#include "xil_cache.h"
#include "xparameters.h"
#include "sleep.h"
#include "bmp.h"
#include "ff.h"
#include "xiicps_hdmi_intr_example.h"
#include "ADV7511_reg.h"
#include "xiic.h"
#include "xiic_l.h"
#include "xvprocss.h"
#include "xil_printf.h"

// XPAR redefines
//#define DYNCLK_BASEADDR XPAR_AXI_DYNCLK_0_BASEADDR
#define VGA_VDMA_ID XPAR_AXIVDMA_0_DEVICE_ID
#define DISP_VTC_ID XPAR_VTC_0_DEVICE_ID
#define VID_VTC_IRPT_ID XPS_FPGA3_INT_ID
#define VID_GPIO_IRPT_ID XPS_FPGA4_INT_ID
#define SCU_TIMER_ID XPAR_SCUTIMER_DEVICE_ID
#define UART_BASEADDR XPAR_PS7_UART_1_BASEADDR

#define IIC_BASE_ADDRESS XPS_I2C0_BASEADDR

/* ------------------------------------------------------------ */
/*				Global Variables								*/
/* ------------------------------------------------------------ */


// Display Driver structs
DisplayCtrl dispCtrl;
XAxiVdma vdma;

static FIL fil;		// File object
static FATFS fatfs;

// Framebuffers for video data
u8 frameBuf[DISPLAY_NUM_FRAMES][DEMO_MAX_FRAME] __attribute__ ((aligned(64)));
u8 *pFrames[DISPLAY_NUM_FRAMES]; //array of pointers to the frame buffers

//XIicPs IicInstance;


//----------- START NEW IIC CODE >>>>>>>>>>>>>>>>>

#define PAGE_SIZE2 16

#define IIC_BASE_ADDRESS XPS_I2C0_BASEADDR

//#define EEPROM_TEST_START_ADDRESS 0x80

#define IIC_SWITCH_ADDRESS  0x74
#define IIC_ADV7511_ADDRESS 0x39

#define ADV7511_HPD_CTRL_MASK 0x40
#define ADV7511_HDP_REG_ADDR  0x42

typedef struct {
  u8 addr;
  u8 data;
  u8 init;
} HDMI_REG;

#define NUMBER_OF_HDMI_REGS 41
HDMI_REG hdmi_iic[NUMBER_OF_HDMI_REGS] = {
  {0x41, 0x00, 0x10},
  {0x98, 0x00, 0x03},
  {0x9A, 0x00, 0xE0},
  {0x9C, 0x00, 0x30},
  {0x9D, 0x00, 0x61},
  {0xA2, 0x00, 0xA4},
  {0xA3, 0x00, 0xA4},
  {0xE0, 0x00, 0xD0},
  {0xF9, 0x00, 0x00},
  {0x18, 0x00, 0xE7},
  {0x55, 0x00, 0x00},
  {0x56, 0x00, 0x28},
  {0xD6, 0x00, 0xC0},
  {0xAF, 0x00, 0x04},
  {0xF9, 0x00, 0x00},

  {0x01, 0x00, 0x00},
  {0x02, 0x00, 0x18},
  {0x03, 0x00, 0x00},
  {0x15, 0x00, 0x00},
  {0x16, 0x00, 0x61},
  {0x18, 0x00, 0x46},
  {0x40, 0x00, 0x80},
  {0x41, 0x00, 0x10},
  {0x48, 0x00, 0x48},
  {0x48, 0x00, 0xA8},
  {0x4C, 0x00, 0x06},
  {0x55, 0x00, 0x00},
  {0x55, 0x00, 0x08},
  {0x96, 0x00, 0x20},
  {0x98, 0x00, 0x03},
  {0x98, 0x00, 0x02},
  {0x9C, 0x00, 0x30},
  {0x9D, 0x00, 0x61},
  {0xA2, 0x00, 0xA4},
  {0x43, 0x00, 0xA4},
  {0xAF, 0x00, 0x16},
  {0xBA, 0x00, 0x60},
  {0xDE, 0x00, 0x9C},
  {0xE4, 0x00, 0x60},
  {0xFA, 0x00, 0x7D}

};

u8 EepromIicAddr;
u8 EepromReadByte(AddressType Address, u8 *BufferPtr, u8 ByteCount);
u8 EepromWriteByte(AddressType Address, u8 *BufferPtr, u8 ByteCount);

int start_iic_hdmi_2() {
  u8 BytesRead;
  u32 StatusReg;
  u8 Index;
  int Status;
  u32 i;
  u8 channel[1] = {0x02};

  xil_printf("\r\nBefore XIic_DynInit\r\n");
  Xil_Out32(XPS_GPIO_BASEADDR + 0x204, 0x2000);  //Xil_Out32(0xe000a204, 0x2000);
  Xil_Out32(XPS_GPIO_BASEADDR + 0x208, 0x2000);  //Xil_Out32(0xe000a208, 0x2000);
  Xil_Out32(XPS_GPIO_BASEADDR + 0x040, 0x2000);  //Xil_Out32(0xe000a040, 0x2000);

  Status = XIic_DynInit(IIC_BASE_ADDRESS);  if (Status != XST_SUCCESS) { return XST_FAILURE; } else { xil_printf("FAILED XIic_DynInit\n\r"); }
  xil_printf("\r\nAfter XIic_DynInit\r\n");

  while(((StatusReg = XIic_ReadReg(IIC_BASE_ADDRESS,
      XIIC_SR_REG_OFFSET)) & 
      (XIIC_SR_RX_FIFO_EMPTY_MASK | 
      XIIC_SR_TX_FIFO_EMPTY_MASK |
      XIIC_SR_BUS_BUSY_MASK)) !=
      (XIIC_SR_RX_FIFO_EMPTY_MASK |
      XIIC_SR_TX_FIFO_EMPTY_MASK)) {
  }

  EepromIicAddr = IIC_SWITCH_ADDRESS;
  XIic_DynSend(IIC_BASE_ADDRESS, EepromIicAddr, channel, sizeof(channel), XIIC_STOP);

  EepromIicAddr = IIC_ADV7511_ADDRESS;
  for (Index=0; Index < NUMBER_OF_HDMI_REGS; Index++) {
    EepromWriteByte(hdmi_iic[Index].addr, &hdmi_iic[Index].init, 1);
  }

  for (Index=0; Index<NUMBER_OF_HDMI_REGS; Index++) {
    BytesRead = EepromReadByte(hdmi_iic[Index].addr, &hdmi_iic[Index].data, 1);
    for(i=0; i<1000; i++) {};
    if(BytesRead != 1) {
      return XST_FAILURE;
    } else {
      xil_printf("ERROR: Expected BytesRead==1, actual BytesRead==%0d \n\r",BytesRead);
    }
  }
  return XST_SUCCESS;
}

u8 EepromReadByte(AddressType Address, u8 *BufferPtr, u8 ByteCount) {
  u8 ReceivedByteCount;
  u8 SentByteCount;
  u16 StatusReg;

  do {
    StatusReg = XIic_ReadReg(IIC_BASE_ADDRESS, XIIC_SR_REG_OFFSET);
    if(!(StatusReg & XIIC_SR_BUS_BUSY_MASK)) {
      SentByteCount = XIic_DynSend(IIC_BASE_ADDRESS, EepromIicAddr, (u8 *) &Address, sizeof(Address), XIIC_REPEATED_START);
    }
  } while (SentByteCount != sizeof(Address));

  ReceivedByteCount = XIic_DynRecv(IIC_BASE_ADDRESS, EepromIicAddr, BufferPtr, ByteCount);
  return ReceivedByteCount;
}

u8 EepromWriteByte(AddressType Address, u8 *BufferPtr, u8 ByteCount) {
  u8 SentByteCount;
  u8 WriteBuffer[sizeof(Address) + PAGE_SIZE2];
  u8 Index;

  if (sizeof(AddressType) ==2) {
    WriteBuffer[0] = (u8) (Address >> 8);
    WriteBuffer[1] = (u8) (Address);
  }
  else if (sizeof(AddressType) == 1) {
    WriteBuffer[0] = (u8) (Address);
    //EepromIicAddr |= (EEPROM_TEST_START_ADDRESS >> 8) & 0X7;
  }

  for (Index = 0; Index < ByteCount; Index++) {
    WriteBuffer[sizeof(Address) + Index] = BufferPtr[Index];
  }

  SentByteCount = XIic_DynSend(IIC_BASE_ADDRESS, EepromIicAddr, WriteBuffer, sizeof(Address) + ByteCount, XIIC_STOP);
  return SentByteCount - sizeof(Address);

}

int check_hdmi_hpd_status2(void) {
   u8 data = 0x00;
   u8 BytesRead;

   BytesRead = EepromReadByte(0x42, &data, 1);
   
   if((data & ADV7511_HPD_CTRL_MASK)  == ADV7511_HPD_CTRL_MASK) {
     xil_printf("Monitor Connected\n\r");
     return 1;
   }
   else  {
      xil_printf("Montor not Connected\n\r");
      return 0;
   }
}

//----------- END NEW IIC CODE <<<<<<<<<<<<<<<<

/* ------------------------------------------------------------ */
/*				Procedure Definitions							*/
/* ------------------------------------------------------------ */

unsigned char read_line_buf[1920 * 3];
unsigned char Write_line_buf[1920 * 3];
void bmp_read(char * bmp,u8 *frame,u32 stride) {
	short y, x;
	short Ximage;
	short Yimage;
	u32 iPixelAddr = 0;
	FRESULT res;
	unsigned char TMPBUF[64];
	unsigned int br;         // File R/W count

	res = f_open(&fil, bmp, FA_OPEN_EXISTING | FA_READ);
	if(res != FR_OK) {
		return ;
	}
	res = f_read(&fil, TMPBUF, 54, &br);
	if(res != FR_OK) {
		return ;
	}
	Ximage=(unsigned short int)TMPBUF[19]*256+TMPBUF[18];
	Yimage=(unsigned short int)TMPBUF[23]*256+TMPBUF[22];
	iPixelAddr = (Yimage-1)*stride ;

	for(y = 0; y < Yimage ; y++) {
		f_read(&fil, read_line_buf, Ximage * 3, &br);
		for(x = 0; x < Ximage; x++) {
			frame[x * BYTES_PIXEL + iPixelAddr + 0] = read_line_buf[x * 3 + 0];
			frame[x * BYTES_PIXEL + iPixelAddr + 1] = read_line_buf[x * 3 + 1];
			frame[x * BYTES_PIXEL + iPixelAddr + 2] = read_line_buf[x * 3 + 2];
		}
		iPixelAddr -= stride;
	}
	f_close(&fil);
}

/************************** Function Definitions *****************************/


int vpss_csc_rgb_to_ycrcb(void) {
	Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x10, 0x0);   // in rgb
	Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x18, 0x2);   // out 422
	Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x20, 0x780); // width
	Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x28, 0x438); // height

	s32 scale_factor = (1<<12);
    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x50, (s32) ( 0.2568*(float)scale_factor));  //K11
    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x60, (s32) ( 0.5041*(float)scale_factor));  //K12  swapped |
    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x58, (s32) ( 0.0979*(float)scale_factor));  //K13  swapped |

    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x68, (s32) (-0.1482*(float)scale_factor));  //K21
    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x78, (s32) (-0.2910*(float)scale_factor));  //K22  swapped |
    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x70, (s32) ( 0.4393*(float)scale_factor));  //K23  swapped |

    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x80, (s32) ( 0.4393*(float)scale_factor));  //K31
    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x90, (s32) (-0.3678*(float)scale_factor));  //K32  swapped |
    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x88, (s32) (-0.0714*(float)scale_factor));  //K33  swapped |


    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x98, (s32) (16) );  // g
    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0xa0, (s32) (128) ); // b
    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0xa8, (s32) (128) ); // r

	Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0xb0, 0x00); // clamp minimum
	Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0xb8, 0xff); // clamp maximum
    //xil_printf("        0xa8            XPAR_V_PROC_SS_0_BASEADDR: = %x\n\r", Xil_In32(XPAR_V_PROC_SS_0_BASEADDR + 0xa8       ));
    Xil_Out32(XPAR_V_PROC_SS_0_BASEADDR + 0x0, 0x81);  // start
}


void noise_insert(u32 ena, u32 seed, u32 luma, u32 chroma) {
  // NOISE GEN SEEDS 0..15
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x00, seed+0x00A0 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x04, seed+0x00B1 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x08, seed+0x00C2 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x0C, seed+0x00D3 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x10, seed+0x00E4 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x14, seed+0x00F5 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x18, seed+0x0006 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x1C, seed+0x0017 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x20, seed+0x0028 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x24, seed+0x0039 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x28, seed+0x004A );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x2C, seed+0x005B );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x30, seed+0x006C );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x34, seed+0x007D );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x38, seed+0x008E );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x3C, seed+0x009F );

  // NOISE GEN PROB 0..15
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x40, luma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x44, luma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x48, luma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x4C, luma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x50, luma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x54, luma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x58, luma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x5C, luma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x60, chroma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x64, chroma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x68, chroma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x6C, chroma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x70, chroma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x74, chroma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x78, chroma);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x7C, chroma);

  // NOISE GEN CTRL (ON/OFF)
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x80, 0x2); // load seeds
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x80, ena);
}


void noise_insert_l(u32 ena, u32 seed, u32 luma, u32 chroma) {
	  // NOISE GEN SEEDS 0..15
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x00, seed+0x00A0 );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x04, seed+0x00B1 );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x08, seed+0x00C2 );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x0C, seed+0x00D3 );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x10, seed+0x00E4 );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x14, seed+0x00F5 );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x18, seed+0x0006 );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x1C, seed+0x0017 );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x20, seed+0x0028 );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x24, seed+0x0039 );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x28, seed+0x004A );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x2C, seed+0x005B );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x30, seed+0x006C );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x34, seed+0x007D );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x38, seed+0x008E );
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x3C, seed+0x009F );

	  // NOISE GEN PROB 0..15
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x40, 0);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x44, 0);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x48, 0);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x4C, 0);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x50, luma);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x54, luma);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x58, 0);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x5C, 0);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x60, 0);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x64, chroma);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x68, chroma);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x6C, chroma);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x70, 0);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x74, 0);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x78, 0);
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x7C, 0);

	  // NOISE GEN CTRL (ON/OFF)
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x80, 0x2); // load seeds
	  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x80, ena);

}

void noise_insert_all(u32 ena, u32 seed, u32 luma, u32 chroma, u32 correct) {
  // NOISE GEN SEEDS 0..15
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x00, seed+0x00A0 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x04, seed+0x00B1 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x08, seed+0x00C2 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x0C, seed+0x00D3 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x10, seed+0x00E4 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x14, seed+0x00F5 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x18, seed+0x0006 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x1C, seed+0x0017 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x20, seed+0x0028 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x24, seed+0x0039 );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x28, seed+0x004A );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x2C, seed+0x005B );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x30, seed+0x006C );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x34, seed+0x007D );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x38, seed+0x008E );
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x3C, seed+0x009F );
  // SEEDS 16...31 FOR SECOND NOISE GENERATOR
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x00, seed+0x00A5 );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x04, seed+0x007A );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x08, seed+0x00C3 );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x0C, seed+0x00A3 );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x10, seed+0x00E8 );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x14, seed+0x00B5 );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x18, seed+0x0007 );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x1C, seed+0x0008 );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x20, seed+0x0021 );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x24, seed+0x0079 );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x28, seed+0x00A4 );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x2C, seed+0x008B );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x30, seed+0x00C3 );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x34, seed+0x007C );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x38, seed+0x008C );
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x3C, seed+0x009C );


  // NOISE GEN PROB 0..15
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x40, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x44, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x48, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x4C, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x50, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x54, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x58, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x5C, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x60, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x64, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x68, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x6C, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x70, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x74, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x78, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x7C, 0);

  // 16 ... 31
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x40, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x44, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x48, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x4C, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x50, chroma);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x54, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x58, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x5C, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x60, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x64, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x68, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x6C, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x70, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x74, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x78, 0);
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x7C, 0);

  // NOISE GEN 0 CTRL (ON/OFF)
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x80, 0x2); // load seeds
  Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x80, ena);
  // NOISE GEN 1 CTRL (ON/OFF)
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x80, 0x2); // load seeds
  Xil_Out32(XPAR_NOISE_GENERATOR_1_CTRL_BASEADDR + 0x80, ena);

  // HAMMING DECODER CTRL (ON/OFF)
  Xil_Out32(XPAR_DECODER_HAMMING_0_CTRL_S_BASEADDR + 0x00, correct);


}


/*****************************************************************************/
/**
* This function writes, reads, and verifies the data to the HDMI I2C. It
* does the write as a single page write, performs a buffered read.
*
* @param	None.
*
* @return	XST_SUCCESS if successful else XST_FAILURE.
*
* @note		None.
*
******************************************************************************/
int start_iic_hdmi(void) {
	u32 Index;
	u16 i;
	int Status;
	XIicPs_Config *ConfigPtr;	// Pointer to configuration data

    xil_printf("\n\r********************************************************");
    xil_printf("\n\r********************************************************");
    xil_printf("\n\r** IIC Read ADV7511 Programmable Clock Interrupt Test **");
    xil_printf("\n\r********************************************************");
    xil_printf("\n\r********************************************************\r\n");

	// Initialize the IIC driver so that it is ready to use.
	ConfigPtr = XIicPs_LookupConfig(IIC_DEVICE_ID);
	if (ConfigPtr == NULL) {
		return XST_FAILURE;
	}

	Status = XIicPs_CfgInitialize(&IicInstance, ConfigPtr,
					ConfigPtr->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	// Setup the Interrupt System.
	Status = SetupInterruptSystem(&IicInstance);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Setup the handlers for the IIC that will be called from the
	 * interrupt context when data has been sent and received, specify a
	 * pointer to the IIC driver instance as the callback reference so
	 * the handlers are able to access the instance data.
	 */
	XIicPs_SetStatusHandler(&IicInstance, (void *) &IicInstance, Handler);

	// Set the IIC serial clock rate.
	XIicPs_SetSClk(&IicInstance, IIC_SCLK_RATE);

    //-------------------------------------------
    // initialize HDMI Controller (ADV7511)
    //-------------------------------------------
	Status = MuxInit(1); if (Status != XST_SUCCESS) { return XST_FAILURE;} // Set the channel value in IIC Mux.

    for (i=0; i<ADV7511_TOTAL_REG; i++) {
      WriteBuffer[0] = (u8)adv7511_registers[i].address;
      WriteBuffer[1] = (u8)adv7511_registers[i].value;
      xil_printf("HDMI Reg WR %02X %02X      ", WriteBuffer[0], WriteBuffer[1]);
      // Write to the HDMI I2C.
      Status = IicWriteData((sizeof(AddressType) + PAGE_SIZE),    IIC_HDMI_ADDR);  if (Status != XST_SUCCESS) { return XST_FAILURE; }
      Status = IicReadData(ReadBuffer, WriteBuffer[0], PAGE_SIZE, IIC_HDMI_ADDR);  if (Status != XST_SUCCESS) { return XST_FAILURE; }
      xil_printf("HDMI Reg RD Addr %02X, %02X %02X\r\n", WriteBuffer[0], ReadBuffer[0], ReadBuffer[1]);
    }
/*
        //-------------------------------------------
        // initialize EEPROM
        //-------------------------------------------
	Status = MuxInit(2); if (Status != XST_SUCCESS) { return XST_FAILURE;} // Set the channel value in IIC Mux.
        for (i=0; i<ADV7511_TOTAL_REG; i++) {
           WriteBuffer[0] = (u8)adv7511_registers[i].address;
           WriteBuffer[1] = (u8)adv7511_registers[i].value;
           xil_printf("EEPROM Reg WR %02X %02X      ", WriteBuffer[0], WriteBuffer[1]);
           // Write to the HDMI I2C.
           Status = IicWriteData((sizeof(AddressType) + PAGE_SIZE),    IIC_EEPROM_ADDR); if (Status != XST_SUCCESS) { return XST_FAILURE; }
           Status = IicReadData(ReadBuffer, WriteBuffer[0], PAGE_SIZE, IIC_EEPROM_ADDR); if (Status != XST_SUCCESS) { return XST_FAILURE; }
           xil_printf("EEPROM Reg RD Addr %02X, %02X %02X\r\n", WriteBuffer[0], ReadBuffer[0], ReadBuffer[1]);
        }

        //-------------------------------------------
        // initialize HDMI Controller (ADV7511)
        //-------------------------------------------
	Status = MuxInit(1); if (Status != XST_SUCCESS) { return XST_FAILURE;} // Set the channel value in IIC Mux.
        for (i=0; i<ADV7511_TOTAL_REG; i++) {
           WriteBuffer[0] = (u8)adv7511_registers[i].address;
           WriteBuffer[1] = (u8)adv7511_registers[i].value;
           xil_printf("HDMI Reg WR %02X %02X      ", WriteBuffer[0], WriteBuffer[1]);
           // Write to the HDMI I2C.
           Status = IicWriteData((sizeof(AddressType) + PAGE_SIZE),    IIC_HDMI_ADDR);  if (Status != XST_SUCCESS) { return XST_FAILURE; }
           Status = IicReadData(ReadBuffer, WriteBuffer[0], PAGE_SIZE, IIC_HDMI_ADDR);  if (Status != XST_SUCCESS) { return XST_FAILURE; }
           xil_printf("HDMI Reg RD Addr %02X, %02X %02X\r\n", WriteBuffer[0], ReadBuffer[0], ReadBuffer[1]);
        }
*/

	xil_printf("Successfully ran IIC ADV7511 Interrupt Example Test\r\n");
	return XST_SUCCESS;
}

/*****************************************************************************/
/**
* This function writes a buffer of data to the IIC serial HDMI.
*
* @param	ByteCount contains the number of bytes in the buffer to be
*		written.
*
* @return	XST_SUCCESS if successful else XST_FAILURE.
*
* @note		The Byte count should not exceed the page size of the HDMI I2C as
*		noted by the constant PAGE_SIZE.
*
******************************************************************************/
int IicWriteData(u16 ByteCount, u8 slave_address) {

	TransmitComplete = FALSE;

	// Send the Data.
	XIicPs_MasterSend(&IicInstance, WriteBuffer,
			   ByteCount, slave_address);

	/*
	 * Wait for the entire buffer to be sent, letting the interrupt
	 * processing work in the background, this function may get
	 * locked up in this loop if the interrupts are not working
	 * correctly.
	 */
	while (TransmitComplete == FALSE) {
		if (0 != TotalErrorCount) {
			return XST_FAILURE;
		}
	}

	// Wait until bus is idle to start another transfer.
	while (XIicPs_BusIsBusy(&IicInstance));

	/*
	 * Wait for a bit of time to allow the programming to occur as reading
	 * the status while programming causes it to fail because of noisy power
	 * on the board containing the HDMI.
	 */
	usleep(20000);
	return XST_SUCCESS;
}

/*****************************************************************************/
/**
* This function reads data from the IIC serial HDMI into a specified buffer.
*
* @param	BufferPtr contains the address of the data buffer to be filled.
* @param	ByteCount contains the number of bytes in the buffer to be read.
*
* @return	XST_SUCCESS if successful else XST_FAILURE.
*
* @note		None.
*
******************************************************************************/
int IicReadData(u8 *BufferPtr, u8 Address, u16 ByteCount, u8 slave_address) {
	int Status;
	//AddressType Address = HDMI_START_ADDRESS;

	// Position the Pointer in HDMI.
	if (sizeof(Address) == 1) {
		WriteBuffer[0] = (u8) (Address);
	}
	else {
		WriteBuffer[0] = (u8) (Address >> 8);
		WriteBuffer[1] = (u8) (Address);
	}

	Status = IicWriteData(sizeof(Address), slave_address);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	ReceiveComplete = FALSE;

	// Receive the Data.
	XIicPs_MasterRecv(&IicInstance, BufferPtr,
			   ByteCount, slave_address);

	while (ReceiveComplete == FALSE) {
		if (0 != TotalErrorCount) {
			return XST_FAILURE;
		}
	}

	// Wait until bus is idle to start another transfer.
	while (XIicPs_BusIsBusy(&IicInstance));

	return XST_SUCCESS;
}

/******************************************************************************/
/**
*
* This function setups the interrupt system such that interrupts can occur
* for the IIC.
*
* @param	IicPsPtr contains a pointer to the instance of the Iic
*		which is going to be connected to the interrupt controller.
*
* @return	XST_SUCCESS if successful, otherwise XST_FAILURE.
*
* @note		None.
*
*******************************************************************************/
static int SetupInterruptSystem(XIicPs *IicPsPtr) {
	int Status;
	XScuGic_Config *IntcConfig; // Instance of the interrupt controller

	Xil_ExceptionInit();

	// Initialise the interrupt controller driver so that it is ready to use.
	IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
	if (NULL == IntcConfig) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(&InterruptController, IntcConfig,
					IntcConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	// Connect the interrupt controller interrupt handler to the hardware
	// interrupt handling logic in the processor.
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_IRQ_INT,
				(Xil_ExceptionHandler)XScuGic_InterruptHandler,
				&InterruptController);

	/*
	 * Connect the device driver handler that will be called when an
	 * interrupt for the device occurs, the handler defined above performs
	 * the specific interrupt processing for the device.
	 */
	Status = XScuGic_Connect(&InterruptController, IIC_INTR_ID,
			(Xil_InterruptHandler)XIicPs_MasterInterruptHandler,
			(void *)IicPsPtr);
	if (Status != XST_SUCCESS) {
		return Status;
	}

	// Enable the interrupt for the Iic device.
	XScuGic_Enable(&InterruptController, IIC_INTR_ID);

	// Enable interrupts in the Processor.
	Xil_ExceptionEnable();

	return XST_SUCCESS;
}

/*****************************************************************************/
/**
*
* This function is the handler which performs processing to handle data events
* from the IIC.  It is called from an interrupt context such that the amount
* of processing performed should be minimised.
*
* This handler provides an example of how to handle data for the IIC and
* is application specific.
*
* @param	CallBackRef contains a callback reference from the driver, in
*		this case it is the instance pointer for the IIC driver.
* @param	Event contains the specific kind of event that has occurred.
* @param	EventData contains the number of bytes sent or received for sent
*		and receive events.
*
* @return	None.
*
* @note		None.
*
*******************************************************************************/
void Handler(void *CallBackRef, u32 Event) {

	// All of the data transfer has been finished
	if (0 != (Event & XIICPS_EVENT_COMPLETE_RECV)){
		ReceiveComplete = TRUE;
	}
	else if (0 != (Event & XIICPS_EVENT_COMPLETE_SEND)) {
		TransmitComplete = TRUE;
	}
	else if (0 == (Event & XIICPS_EVENT_SLAVE_RDY)){
		/*
		 * If it is other interrupt but not slave ready interrupt, it is
		 * an error.
		 * Data was received with an error.
		 */
		TotalErrorCount++;
	}
}

/*****************************************************************************/
/**
* This function initialises the IIC MUX to select HDMI.
*
* @param	None.
*
* @return	XST_SUCCESS if pass, otherwise XST_FAILURE.
*
* @note		None.
*
****************************************************************************/
int MuxInit(u8 position) {
	u8 WriteBufferMux;
	u8 MuxIicAddr = IIC_MUX_ADDRESS;
	u8 Buffer = 0;
    int Status;

    //Status = XIic_DynInit(IIC_BASE_ADDRESS); if (Status != XST_SUCCESS) { return XST_FAILURE; }

	// GPIO Code to pull MUX out of reset. // XPS_GPIO_BASEADDR, XPS_I2C0_BASEADDR
    // Xil_Out32(XPS_GPIO_BASEADDR + 0x204, 0x2000);  //Xil_Out32(0xe000a204, 0x2000);
    // Xil_Out32(XPS_GPIO_BASEADDR + 0x208, 0x2000);  //Xil_Out32(0xe000a208, 0x2000);
    // Xil_Out32(XPS_GPIO_BASEADDR + 0x040, 0x2000);  //Xil_Out32(0xe000a040, 0x2000);

    // Channel select value for ADV7511
	WriteBufferMux = (1<<position);

	TransmitComplete = FALSE;

	// Send the Data.
	XIicPs_MasterSend(&IicInstance, &WriteBufferMux,1, MuxIicAddr);
	while (TransmitComplete == FALSE) {
		if (0 != TotalErrorCount) {
			return XST_FAILURE;
		}
	}

	// Wait until bus is idle to start another transfer.
	while (XIicPs_BusIsBusy(&IicInstance));

	ReceiveComplete = FALSE;

	// Receive the Data.
	XIicPs_MasterRecv(&IicInstance, &Buffer,1, MuxIicAddr);
	while (ReceiveComplete == FALSE) {
		if (0 != TotalErrorCount) {
			return XST_FAILURE;
		}
	}

	// Wait until bus is idle to start another transfer.
	while (XIicPs_BusIsBusy(&IicInstance));
	return XST_SUCCESS;
}


int main(void) {

	int Status;
	XAxiVdma_Config *vdmaConfig;
	int i;
	FRESULT rc;
	for (i = 0; i < DISPLAY_NUM_FRAMES; i++) { // Initialise an array of pointers to the 3 frame buffers
		pFrames[i] = frameBuf[i];
	}

	vdmaConfig = XAxiVdma_LookupConfig(VGA_VDMA_ID); // Initialise VDMA driver
	if (!vdmaConfig) {
		xil_printf("No video DMA found for ID %d\r\n", VGA_VDMA_ID);
	}
	else {
		xil_printf("Found Video DMA for ID %d\r\n", VGA_VDMA_ID);
	}

	Status = XAxiVdma_CfgInitialize(&vdma, vdmaConfig, vdmaConfig->BaseAddress);
	if (Status != XST_SUCCESS) {
		xil_printf("VDMA Configuration Initialisation failed %d\r\n", Status);
	}
	else {
		xil_printf("VDMA Configuration Initialisation PASSED %d\r\n", Status);
	}

	Status = start_iic_hdmi();
	if (Status != XST_SUCCESS) {
		xil_printf("I2C and HDMI controller initialisation failed during demo initialisation%d\r\n", Status);
	}
	else {
		xil_printf("I2C and HDMI controller initialisation PASSED during demo initialisation%d\r\n", Status);
	}

/*
	Status = start_iic_hdmi_2();
	if (Status != XST_SUCCESS) {
	   xil_printf("ADV7511 IIC programming FAILED\r\n");
	   return XST_FAILURE;
	}
	xil_printf("ADV7511 IIC programming PASSED");
 	check_hdmi_hpd_status2();
*/

    // VPSS Configuration CSC only, RGB to YCRCB
	vpss_csc_rgb_to_ycrcb();

	Status = DisplayInitialize(&dispCtrl, &vdma, DISP_VTC_ID, pFrames, DEMO_STRIDE); // Initialise the Display controller and start it
	if (Status != XST_SUCCESS) {
		xil_printf("Display Ctrl initialisation failed during demo initialisation%d\r\n", Status);
	}
	else {
		xil_printf("Display Ctrl initialisation PASSED during demo initialisation%d\r\n", Status);
	}

	Status = DisplayStart(&dispCtrl);
	if (Status != XST_SUCCESS) {
		xil_printf("Couldn't start display during demo initialisation%d\r\n", Status);
	}
	else {
		xil_printf("Start display during demo initialisation PASSED%d\r\n", Status);
	}

	rc = f_mount(&fatfs, "0:/", 0);
	if (rc != FR_OK){
		return 0 ;
	}
	else {
		xil_printf("SD card mounted successfully%d\r\n", Status);
	}
    //init i2c, hdmi
	//Xil_Out32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x40, 0xabcdef01);
    //xil_printf("addr = XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x40, data = %x\n\r", Xil_In32(XPAR_NOISE_GENERATOR_0_CTRL_BASEADDR + 0x40));

	xil_printf("Starting BMP read..%d\r\n", Status);
	bmp_read("n.bmp",dispCtrl.framePtr[dispCtrl.curFrame], DEMO_STRIDE);
	xil_printf("Finished BMP read%d\r\n", Status);
	Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);

    // NOISE GENERATION AND CORRECTION OFF

	usleep(5000000);
    for (int j = 0; j < 128; j++ ) {
		xil_printf("No noise, no correction START ..%d\r\n", j);
		noise_insert_all(0 /*noise_enable*/, j/*seed*/, j/*luma err prob*/, j/*chroma err prob*/, 0 /*correction_enable*/);
    	Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);
    	usleep(200000);

		//xil_printf("After read - Stopping noise insertion..%d\r\n", Status);
    	//noise_insert(0 /*master_enable*/, 0/*seed*/, j/*luma err prob*/, 0x00/*chroma err prob*/);
    	//Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);
		//usleep(5000000);
    }
	xil_printf("After read - no noise no correction END ..%d\r\n", Status);
	noise_insert_all(0 /*master_enable*/, 0x00/*seed*/, 0x00/*luma err prob*/, 0x00/*chroma err prob*/, 0);
	Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);
	usleep(5000000);

    // NOISE GENERATION WITH CORRECTION OFF

	usleep(5000000);
    for (int j = 0; j < 128; j++ ) {
		xil_printf("After read - Starting noise insertion, correction off ..%d\r\n", j);
		noise_insert_all(1 /*noise_enable*/, j/*seed*/, j/*luma err prob*/, j/*chroma err prob*/, 0 /*correction_enable*/);
    	Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);
    	usleep(200000);

		//xil_printf("After read - Stopping noise insertion..%d\r\n", Status);
    	//noise_insert(0 /*master_enable*/, 0/*seed*/, j/*luma err prob*/, 0x00/*chroma err prob*/);
    	//Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);
		//usleep(5000000);
    }
	xil_printf("After read - Stopping noise insertion and correction..%d\r\n", Status);
	noise_insert_all(0 /*master_enable*/, 0x00/*seed*/, 0x00/*luma err prob*/, 0x00/*chroma err prob*/, 0);
	Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);
	usleep(5000000);

	// NOISE GENERATION WITH NOISE ON

	usleep(5000000);
    for (int j = 0; j < 128; j++ ) {
		xil_printf("After read - Starting noise insertion, correction on 1 bit (MSB) ..%d\r\n", j);
		noise_insert_all(1 /*master_enable*/, j/*seed*/, j/*luma err prob*/, j/*chroma err prob*/, 1 /*correction*/);
    	Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);
    	usleep(200000);

		//xil_printf("After read - Stopping noise insertion..%d\r\n", Status);
    	//noise_insert(0 /*master_enable*/, 0/*seed*/, j/*luma err prob*/, 0x00/*chroma err prob*/);
    	//Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);
		//usleep(5000000);
    }
	xil_printf("After read - Stopping noise insertion and correction..%d\r\n", Status);
	noise_insert_all(0 /*master_enable*/, 0x00/*seed*/, 0x00/*luma err prob*/, 0x00/*chroma err prob*/, 0);
	Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);
	usleep(5000000);




	//OLD NOISE GENERATOR

	usleep(5000000);
    for (int j = 0; j < 128; j++ ) {
		xil_printf("After read - Starting noise insertion ..%d\r\n", j);
		noise_insert_l(1 /*master_enable*/, j/*seed*/, j/*luma err prob*/, j/*chroma err prob*/);
    	Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);
    	usleep(200000);

		//xil_printf("After read - Stopping noise insertion..%d\r\n", Status);
    	//noise_insert(0 /*master_enable*/, 0/*seed*/, j/*luma err prob*/, 0x00/*chroma err prob*/);
    	//Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);
		//usleep(5000000);
    }
	xil_printf("After read - Stopping noise insertion ..%d\r\n", Status);
	noise_insert_l(0 /*master_enable*/, 0x00/*seed*/, 0x00/*luma err prob*/, 0x00/*chroma err prob*/);
	Xil_DCacheFlushRange((unsigned int) dispCtrl.framePtr[dispCtrl.curFrame], DEMO_MAX_FRAME);
	usleep(5000000);

	return 0;
}
