//#define ADV7511_TOTAL_REG 17 // CSC off
#define ADV7511_TOTAL_REG 42 //CSC on

typedef struct {
    unsigned char address; // 8 bit register address
    unsigned char value; // 8-bit register data
} adv7511_register_t;

//adv7511_register_t;

adv7511_register_t const adv7511_registers[ADV7511_TOTAL_REG] = {
    //power up 
    { 0x41, 0x10 },
    //fix reg 
    { 0x98, 0x03 },
    { 0x9A, 0xE0 },
    { 0x9C, 0x30 },
    { 0x9D, 0x61 },
    { 0xA2, 0xA4 },
    { 0xA3, 0xA4 },
    { 0xE0, 0xD0 },
    { 0xF9, 0x00 },

    //Function 
    { 0x15, 0x21 }, // input [7:4] 0010 = 48.0 kHz, [3:0] ID=1 YCbCr 4:2:2 Separate syncs 
    { 0x16, 0x38 }, // output [7] 0 = 4:4:4, [5:4] 11 = 8 bit, [3:2] 10 = style 1, [0] 0 = RGB
    { 0x17, 0x02 }, // 16:9

    // CSC off
    // { 0x18, 0x00 }, //CSC disable, CSC scaling,
	/*
    // CSC Table 39 HDTV YCbCr (Limited Range) to RGB (Limited Range)
    { 0x18, 0xac }, //A
    { 0x19, 0x53 },
    { 0x1A, 0x08 },
    { 0x1B, 0x00 },
    { 0x1C, 0x00 },
    { 0x1D, 0x00 },
    { 0x1E, 0x19 },
    { 0x1F, 0xD6 },

    { 0x20, 0x1C }, //B
    { 0x21, 0x56 },
    { 0x22, 0x08 },
    { 0x23, 0x00 },
    { 0x24, 0x1E },
    { 0x25, 0x88 },
    { 0x26, 0x02 },
    { 0x27, 0x91 },

    { 0x28, 0x1F }, //C
    { 0x29, 0xFF },
    { 0x2A, 0x08 },
    { 0x2B, 0x00 },
    { 0x2C, 0x0E },
    { 0x2D, 0x85 },
    { 0x2E, 0x18 },
    { 0x2F, 0xBE },

    // CSC Table 40 HDTV YCbCr (Limited Range) to RGB (Full Range
    { 0x18, 0xe7 }, //A
    { 0x19, 0x34 },
    { 0x1A, 0x04 },
    { 0x1B, 0xad },
    { 0x1C, 0x00 },
    { 0x1D, 0x00 },
    { 0x1E, 0x1c },
    { 0x1F, 0x1b },

    { 0x20, 0x1d }, //B
    { 0x21, 0xdc },
    { 0x22, 0x04 },
    { 0x23, 0xad },
    { 0x24, 0x1f },
    { 0x25, 0x24 },
    { 0x26, 0x01 },
    { 0x27, 0x35 },

    { 0x28, 0x00 }, //C
    { 0x29, 0x00 },
    { 0x2A, 0x04 },
    { 0x2B, 0xad },
    { 0x2C, 0x08 },
    { 0x2D, 0x7c },
    { 0x2E, 0x1b },
    { 0x2F, 0x77 },

    // CSC Table 41 SDTV YCbCr (Limited Range) to RGB (Limted Range)
    { 0x18, 0xe6 }, //A
    { 0x19, 0x69 },
    { 0x1A, 0x04 },
    { 0x1B, 0x00 },
    { 0x1C, 0x00 },
    { 0x1D, 0x00 },
    { 0x1E, 0x1a },
    { 0x1F, 0x84 },

    { 0x20, 0x1a }, //B
    { 0x21, 0x64 },
    { 0x22, 0x08 },
    { 0x23, 0x00 },
    { 0x24, 0x1d },
    { 0x25, 0x50 },
    { 0x26, 0x04 },
    { 0x27, 0x23 },

    { 0x28, 0x1f }, //C
    { 0x29, 0xfc },
    { 0x2A, 0x08 },
    { 0x2B, 0x00 },
    { 0x2C, 0x0d },
    { 0x2D, 0xde},
    { 0x2E, 0x19 },
    { 0x2F, 0x13 },

    // CSC Table 42 SDTV YCbCr (Limited Range) to RGB (Limted Range)
    { 0x18, 0xe6 }, //A
    { 0x19, 0x69 },
    { 0x1A, 0x04 },
    { 0x1B, 0xac },
    { 0x1C, 0x00 },
    { 0x1D, 0x00 },
    { 0x1E, 0x1c },
    { 0x1F, 0x81 },

    { 0x20, 0x1c }, //B
    { 0x21, 0xbc },
    { 0x22, 0x04 },
    { 0x23, 0xad },
    { 0x24, 0x1e },
    { 0x25, 0x6e },
    { 0x26, 0x02 },
    { 0x27, 0x20 },

    { 0x28, 0x1f }, //C
    { 0x29, 0xfe },
    { 0x2A, 0x04 },
    { 0x2B, 0xad },
    { 0x2C, 0x08 },
    { 0x2D, 0x1a},
    { 0x2E, 0x1b },
    { 0x2F, 0xa9 },

    // CSC Table 56 HDTV YCbCr (16to 235) to RGB (16to 235)
    { 0x18, 0xac }, //A
    { 0x19, 0x53 },
    { 0x1A, 0x08 },
    { 0x1B, 0x00 },
    { 0x1C, 0x00 },
    { 0x1D, 0x00 },
    { 0x1E, 0x19 },
    { 0x1F, 0xd6 },

    { 0x20, 0x1c }, //B
    { 0x21, 0x56 },
    { 0x22, 0x08 },
    { 0x23, 0x00 },
    { 0x24, 0x1e },
    { 0x25, 0x88 },
    { 0x26, 0x02 },
    { 0x27, 0x91 },

    { 0x28, 0x1f }, //C
    { 0x29, 0xff },
    { 0x2A, 0x08 },
    { 0x2B, 0x00 },
    { 0x2C, 0x0e },
    { 0x2D, 0x85},
    { 0x2E, 0x18 },
    { 0x2F, 0xbe },

    // CSC Table 57 HDTV YCbCr (16to 235) to RGB (0 to 255)
    { 0x18, 0xe7 }, //A
    { 0x19, 0x34 },
    { 0x1A, 0x04 },
    { 0x1B, 0xad },
    { 0x1C, 0x00 },
    { 0x1D, 0x00 },
    { 0x1E, 0x1c },
    { 0x1F, 0x1b },

    { 0x20, 0x1d }, //B
    { 0x21, 0xdc },
    { 0x22, 0x04 },
    { 0x23, 0xad },
    { 0x24, 0x1f },
    { 0x25, 0x24 },
    { 0x26, 0x01 },
    { 0x27, 0x35 },

    { 0x28, 0x00 }, //C
    { 0x29, 0x00 },
    { 0x2A, 0x04 },
    { 0x2B, 0xad },
    { 0x2C, 0x08 },
    { 0x2D, 0x7c},
    { 0x2E, 0x1b },
    { 0x2F, 0x77 },

    // CSC Table 58 SDTV YCbCr (16to 235) to RGB (16 to 235)
    { 0x18, 0xaa }, //A
    { 0x19, 0xf8 },
    { 0x1A, 0x08 },
    { 0x1B, 0x00 },
    { 0x1C, 0x00 },
    { 0x1D, 0x00 },
    { 0x1E, 0x1a },
    { 0x1F, 0x84 },

    { 0x20, 0x1a }, //B
    { 0x21, 0x6a },
    { 0x22, 0x08 },
    { 0x23, 0x00 },
    { 0x24, 0x1d},
    { 0x25, 0x50 },
    { 0x26, 0x04 },
    { 0x27, 0x23 },

    { 0x28, 0x1f }, //C
    { 0x29, 0xfc },
    { 0x2A, 0x08 },
    { 0x2B, 0x00 },
    { 0x2C, 0x0d },
    { 0x2D, 0xde},
    { 0x2E, 0x19 },
    { 0x2F, 0x13 },
*/
    // CSC Table 59 SDTV YCbCr (16to 235) to RGB (0 to 255) - (Default Value)
    { 0x18, 0xe6 }, //A
    { 0x19, 0x69 },
    { 0x1A, 0x04 },
    { 0x1B, 0xac },
    { 0x1C, 0x00 },
    { 0x1D, 0x00 },
    { 0x1E, 0x1c },
    { 0x1F, 0x81 },

    { 0x20, 0x1c }, //B
    { 0x21, 0xbc },
    { 0x22, 0x04 },
    { 0x23, 0xad },
    { 0x24, 0x1e},
    { 0x25, 0x6e },
    { 0x26, 0x02 },
    { 0x27, 0x20 },

    { 0x28, 0x1f }, //C
    { 0x29, 0xfe },
    { 0x2A, 0x04 },
    { 0x2B, 0xad },
    { 0x2C, 0x08 },
    { 0x2D, 0x1a},
    { 0x2E, 0x1b },
    { 0x2F, 0xa9 },

    { 0x56, 0x28 },
    { 0x55, 0x00 },
    { 0x48, 0x08 }, // [4:3] 01 = right justified
    { 0xAF, 0x14 }, // HDMI mode
    { 0x40, 0x80 }, // GC enable
    { 0x4A, 0x10 }, // GC update
};

