# FPGA system for image data error correction

## Gitlab location:

https://gitlab.upt.ro/cristina.vasiliu/fpga-system-for-image-data-error-correction.git


## Vivado (hardware part)

Has the following directory structure:
- constraints (contains constraints for Vivado project)
- hdl (contains the top level wrapper)
- my_ips (contains the custom IPs)
- utils (contains the .dcp archive file, required for regenerating the project)

How to regenerate the block diagram and source files:

1. Clone the git project in a working directory 

2. Open Vivado (version 2023.02)

3. From the tcl command line, change to working directory

4. From the tcl command line, run the following command: source system_script.tcl

5. Project opens and compilation starts (~30 minutes)

6. After the bitstream generation is completed, run the following command in the tcl console: 
   write_hw_platform -fixed -include_bit -force -file sd_hdmi_system/design_1_wrapper.xsa

The .bit (bitstream/programming) file is located at: sd_hdmi_system/sd_hdmi_system.runs/impl_1

The .xsa (exported hardware) file is located at: sd_hdmi_system

## Vitis (software part)

Has the following directory structure:
- video (contains the source files; e.g. main.c can be found in /src)
- video_system (contains the system project)
- design_1_wrapper (the platform, contains the FSBL First Stage Boot Loader)

How to regenerate the software platform:

1. Open Vitis Classic (version 2023.02).

2. Press on 'File' -> 'Import' -> 'Import projects from Git'.

3. Press on 'clone URL'.

3. Put the URL of the project (https://gitlab.upt.ro/cristina.vasiliu/fpga-system-for-image-data-error-correction.git).

4. Select destination folder. 

5. Use default for 'Import existing Eclipse project'. 

6. Once 'finish' is pressed, the platform is imported into Vitis. 

To setup and run the program on the ZC702 development board, see Annex 1 (pg 60) from the thesis.