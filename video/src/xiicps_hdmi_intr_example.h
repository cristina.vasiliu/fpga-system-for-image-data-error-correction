/* $Id: xiicps_hdmi_intr_example.c,v 1.1.2.2 2011/02/01 09:25:19 svemula Exp $ */
/******************************************************************************
*
* (c) Copyright 2010-12 Xilinx, Inc. All rights reserved.
*
* This file contains confidential and proprietary information of Xilinx, Inc.
* and is protected under U.S. and international copyright and other
* intellectual property laws.
*
* DISCLAIMER
* This disclaimer is not a license and does not grant any rights to the
* materials distributed herewith. Except as otherwise provided in a valid
* license issued to you by Xilinx, and to the maximum extent permitted by
* applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL
* FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS,
* IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE;
* and (2) Xilinx shall not be liable (whether in contract or tort, including
* negligence, or under any other theory of liability) for any loss or damage
* of any kind or nature related to, arising under or in connection with these
* materials, including for any direct, or any indirect, special, incidental,
* or consequential loss or damage (including loss of data, profits, goodwill,
* or any type of loss or damage suffered as a result of any action brought by
* a third party) even if such damage or loss was reasonably foreseeable or
* Xilinx had been advised of the possibility of the same.
*
* CRITICAL APPLICATIONS
* Xilinx products are not designed or intended to be fail-safe, or for use in
* any application requiring fail-safe performance, such as life-support or
* safety devices or systems, Class III medical devices, nuclear facilities,
* applications related to the deployment of airbags, or any other applications
* that could lead to death, personal injury, or severe property or
* environmental damage (individually and collectively, "Critical
* Applications"). Customer assumes the sole risk and liability of any use of
* Xilinx products in Critical Applications, subject only to applicable laws
* and regulations governing limitations on product liability.
*
* THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE
* AT ALL TIMES.
*
******************************************************************************/
/*****************************************************************************/
/**
* @file xiicps_hdmi_intr_example.c
*
* This file consists of a interrupt mode design example which uses the Xilinx
* PS IIC device and XIicPs driver to exercise the HDMI.
*
* The XIicPs_MasterSend() API is used to transmit the data and the
* XIicPs_MasterRecv() API is used to receive the data.
*
* The example is tested with a 2Kb/8Kb serial IIC EEPROM (ST M24C02/M24C08).
* The WP pin of this EEPROM is hardwired to ground on the HW in which this
* was tested.
*
* The AddressType should be u8 as the address pointer in the on-board
* EEPROM is 1 bytes.
*
* This code assumes that no Operating System is being used.
*
* @note
*
* None.
*
* <pre>
* MODIFICATION HISTORY:
*
* Ver   Who  Date     Changes
* ----- ---- -------- ---------------------------------------------------------
* 1.00a sdm  03/15/10 First release
* 1.01a sg   04/13/12 Added MuxInit_hdmi function for initialising the IIC Mux
*		      on the ZC702 board and to configure it for accessing
*		      the IIC HDMI.
*                     Updated to use usleep instead of delay loop
*
* </pre>
*
******************************************************************************/

/***************************** Include Files *********************************/

#include "xparameters.h"
#include "sleep.h"
#include "xiicps.h"
#include "xscugic.h"
#include "xil_exception.h"
#include "xil_printf.h"

/************************** Constant Definitions *****************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define IIC_DEVICE_ID	XPAR_XIICPS_0_DEVICE_ID
#define INTC_DEVICE_ID	XPAR_SCUGIC_SINGLE_DEVICE_ID
#define IIC_INTR_ID	XPAR_XIICPS_0_INTR

/*
 * The following constant defines the address of the IIC Slave device on the
 * IIC bus. Note that since the address is only 7 bits, this constant is the
 * address divided by 2.
 */
#define IIC_HDMI_ADDR		0x39 // 0x72<<1
#define IIC_EEPROM_ADDR		0x54
#define IIC_SCLK_RATE		100000
#define IIC_MUX_ADDRESS 	0x74

// The page size determines how much data should be written at a time. The write function should be called with this as a maximum byte count.
#define PAGE_SIZE		1

// The Starting address in the IIC HDMI on which this test is performed.
// ADV7511 Reg 0
#define HDMI_START_ADDRESS	0

/**************************** Type Definitions *******************************/

// The AddressType should be u8 as the address pointer in the on-board HDMI is 1 byte.
typedef u8 AddressType;

/***************** Macros (Inline Functions) Definitions *********************/

/************************** Function Prototypes ******************************/

int IicPsHdmiIntrExample_hdmi(void);
int IicWriteData(u16 ByteCount, u8 slave_address);
int MuxInit(u8 position);
int IicReadData(u8 *BufferPtr, u8 Address, u16 ByteCount, u8 slave_address);

static int SetupInterruptSystem(XIicPs * IicInstPtr);

static void Handler(void *CallBackRef, u32 Event);

/************************** Variable Definitions *****************************/

XIicPs IicInstance;		// The instance of the IIC device.
XScuGic InterruptController;	// The instance of the Interrupt Controller.

// Write buffer for writing a page.
u8 WriteBuffer[sizeof(AddressType) + PAGE_SIZE];

u8 ReadBuffer[PAGE_SIZE];	// Read buffer for reading a page.

volatile u8 TransmitComplete;	// Flag to check completion of Transmission
volatile u8 ReceiveComplete;	// Flag to check completion of Reception
volatile u32 TotalErrorCount;






