
`timescale 1 ns / 1 ps

	module noise_generator_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface ctrl
		parameter integer C_ctrl_DATA_WIDTH	= 32,
		parameter integer C_ctrl_ADDR_WIDTH	= 8,

		// Parameters of Axi Slave Bus Interface in
		parameter integer C_in_TDATA_WIDTH	= 32,

		// Parameters of Axi Master Bus Interface out
		parameter integer C_out_TDATA_WIDTH	= 32,
		parameter integer C_out_START_COUNT	= 32
	)
	(
		// Users to add ports here
		output [15:0][C_ctrl_DATA_WIDTH-1:0]	r_seed,
		output [15:0][C_ctrl_DATA_WIDTH-1:0]	r_prob,
		output       [C_ctrl_DATA_WIDTH-1:0]	r_ctrl,

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface ctrl
		input wire  ctrl_aclk,
		input wire  ctrl_aresetn,
		input wire [C_ctrl_ADDR_WIDTH-1 : 0] ctrl_awaddr,
		input wire [2 : 0] ctrl_awprot,
		input wire  ctrl_awvalid,
		output wire  ctrl_awready,
		input wire [C_ctrl_DATA_WIDTH-1 : 0] ctrl_wdata,
		input wire [(C_ctrl_DATA_WIDTH/8)-1 : 0] ctrl_wstrb,
		input wire  ctrl_wvalid,
		output wire  ctrl_wready,
		output wire [1 : 0] ctrl_bresp,
		output wire  ctrl_bvalid,
		input wire  ctrl_bready,
		input wire [C_ctrl_ADDR_WIDTH-1 : 0] ctrl_araddr,
		input wire [2 : 0] ctrl_arprot,
		input wire  ctrl_arvalid,
		output wire  ctrl_arready,
		output wire [C_ctrl_DATA_WIDTH-1 : 0] ctrl_rdata,
		output wire [1 : 0] ctrl_rresp,
		output wire  ctrl_rvalid,
		input wire  ctrl_rready,

		// Ports of Axi Slave Bus Interface in
		input wire  in_aclk,
		input wire  in_aresetn,
		output wire  in_tready,
		input wire [C_in_TDATA_WIDTH-1 : 0] in_tdata,
		input wire [(C_in_TDATA_WIDTH/8)-1 : 0] in_tstrb,
		input wire  in_tlast,
		input wire  in_tvalid,

		// Ports of Axi Master Bus Interface out
		input wire  out_aclk,
		input wire  out_aresetn,
		output wire  out_tvalid,
		output wire [C_out_TDATA_WIDTH-1 : 0] out_tdata,
		output wire [(C_out_TDATA_WIDTH/8)-1 : 0] out_tstrb,
		output wire  out_tlast,
		input wire  out_tready
	);
// Instantiation of Axi Bus Interface ctrl
	noise_generator_v1_0_ctrl # ( 
		.C_S_AXI_DATA_WIDTH(C_ctrl_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_ctrl_ADDR_WIDTH)
	) noise_generator_v1_0_ctrl_inst (
		.r0(r_seed[0]),
		.r1(r_seed[1]),
		.r2(r_seed[2]),
		.r3(r_seed[3]),
		.r4(r_seed[4]),
		.r5(r_seed[5]),
		.r6(r_seed[6]),
		.r7(r_seed[7]),
		.r8(r_seed[8]),
		.r9(r_seed[9]),
		.r10(r_seed[10]),
		.r11(r_seed[11]),
		.r12(r_seed[12]),
		.r13(r_seed[13]),
		.r14(r_seed[14]),
		.r15(r_seed[15]),
		.r16(r_prob[0]),
		.r17(r_prob[1]),
		.r18(r_prob[2]),
		.r19(r_prob[3]),
		.r20(r_prob[4]),
		.r21(r_prob[5]),
		.r22(r_prob[6]),
		.r23(r_prob[7]),
		.r24(r_prob[8]),
		.r25(r_prob[9]),
		.r26(r_prob[10]),
		.r27(r_prob[11]),
		.r28(r_prob[12]),
		.r29(r_prob[13]),
		.r30(r_prob[14]),
		.r31(r_prob[15]),
		.r32(r_ctrl),
		.S_AXI_ACLK(ctrl_aclk),
		.S_AXI_ARESETN(ctrl_aresetn),
		.S_AXI_AWADDR(ctrl_awaddr),
		.S_AXI_AWPROT(ctrl_awprot),
		.S_AXI_AWVALID(ctrl_awvalid),
		.S_AXI_AWREADY(ctrl_awready),
		.S_AXI_WDATA(ctrl_wdata),
		.S_AXI_WSTRB(ctrl_wstrb),
		.S_AXI_WVALID(ctrl_wvalid),
		.S_AXI_WREADY(ctrl_wready),
		.S_AXI_BRESP(ctrl_bresp),
		.S_AXI_BVALID(ctrl_bvalid),
		.S_AXI_BREADY(ctrl_bready),
		.S_AXI_ARADDR(ctrl_araddr),
		.S_AXI_ARPROT(ctrl_arprot),
		.S_AXI_ARVALID(ctrl_arvalid),
		.S_AXI_ARREADY(ctrl_arready),
		.S_AXI_RDATA(ctrl_rdata),
		.S_AXI_RRESP(ctrl_rresp),
		.S_AXI_RVALID(ctrl_rvalid),
		.S_AXI_RREADY(ctrl_rready)
	);

// Instantiation of Axi Bus Interface in
/*
	noise_generator_v1_0_in # ( 
		.C_S_AXIS_TDATA_WIDTH(C_in_TDATA_WIDTH)
	) noise_generator_v1_0_in_inst (
		.S_AXIS_ACLK(in_aclk),
		.S_AXIS_ARESETN(in_aresetn),
		.S_AXIS_TREADY(in_tready),
		.S_AXIS_TDATA(in_tdata),
		.S_AXIS_TSTRB(in_tstrb),
		.S_AXIS_TLAST(in_tlast),
		.S_AXIS_TVALID(in_tvalid)
	);
*/
// Instantiation of Axi Bus Interface out
/*
	noise_generator_v1_0_out # ( 
		.C_M_AXIS_TDATA_WIDTH(C_out_TDATA_WIDTH),
		.C_M_START_COUNT(C_out_START_COUNT)
	) noise_generator_v1_0_out_inst (
		.M_AXIS_ACLK(out_aclk),
		.M_AXIS_ARESETN(out_aresetn),
		.M_AXIS_TVALID(out_tvalid),
		.M_AXIS_TDATA(out_tdata),
		.M_AXIS_TSTRB(out_tstrb),
		.M_AXIS_TLAST(out_tlast),
		.M_AXIS_TREADY(out_tready)
	);
*/
	// Add user logic here
    wire [15:0][7:0] nums;
	wire master_en, load_seeds;
	
	reg [15:0] flip_nxt, flip_ff;

	assign master_en  = r_ctrl[0];
	assign load_seeds = r_ctrl[1];	
	
	genvar i;
	generate for(i=0; i<16; i=i+1) begin : gen_lfsrs
		
	lfsr_8 lfsr_8_i (
      .clk  (in_aclk    ),
      .rst  (~in_aresetn),
	  .load (load_seeds ),
      .seed (r_seed[i]  ), //input  [7:0] 
      .dat  (nums[i]    )  //output [7:0] 
    );
    
    always @ (*) begin
      flip_nxt[i] = (nums[i] < r_prob[i])? master_en : 0;
    end
    
    always @(posedge in_aclk) begin
      if (~in_aresetn) begin
        flip_ff[i] <= 1'b0;
      end 
      else begin
        flip_ff[i] <= flip_nxt[i];
      end
    end    

    assign out_tdata[i] = /*(in_tvalid & out_tready)?*/ in_tdata[i] ^ flip_ff[i]; // : in_tdata[i]; // insert data error if requested

    end
    endgenerate
    
    assign in_tready =  out_tready;
	assign out_tvalid =  in_tvalid;		

	assign out_tstrb = in_tstrb;
	assign out_tlast = in_tlast;

	// User logic ends

	endmodule
